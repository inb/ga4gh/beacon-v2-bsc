Beacon v2 JEE 10 implementation.

The implementation is based on the Jakarta NoSQL API and relies on MongoDB as a backend.

Compiling:
```shell
>git clone https://gitlab.bsc.es/inb/ga4gh/beacon-v2-bsc.git
>cd beacon-v2-bsc
>mvn install
```
The beacon-nosql-server-2.0.5.war will be generated in the beacon-v2-bsc/beacon-nosql-server/target/ directory

The project is divided into several modules:

- **beacon-nosql-server**: is the Beacon v2 REST API server implementation.
- **beacon-nosql-server-filters**: is a module that is in charge of Beacon's filters configuration.
- **beacon-nosql-server-filters-util**: is an auxiliary tool that prepares ontological terms for usage in **beacon-nosql-server-filters**.
- **beacon-nosql-model**: is a jNoSQL java entities for the ['beacon-data-model'](https://github.com/elixir-europe/java-beacon-v2.api/tree/master/beacon-data-model) model interfaces.
- **vrs-jnosql-model**: is a jNoSQL java entities for the ['GA4GH VRS 1.2'](https://github.com/elixir-europe/java-ga4gh-vrs-model) model.

N.B. 
- this version updates jnosql to 1.0.0.
- it also replaced jnosql queries using mongodb aggregation pipeline directly (although through jnosql).
- "alfanumeric" filters are now supported in addition to the "ontology" ones.
- it includes support of GA4GH VRS 1.2 using jsonb 3.0.0 @Inheritance, @DiscriminatorColumn, etc. features.
- THESE FEATURES ARE BROKEN IN YASSON 3.0.3 @see https://github.com/eclipse-ee4j/yasson/pull/590 (yasson 3.0.4-SNAPSHOT must be used)
