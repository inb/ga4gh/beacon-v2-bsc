/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.nosql;

import es.bsc.inb.ga4gh.beacon.model.v200.dataset.Dataset;
import jakarta.json.JsonObject;
import jakarta.nosql.Column;
import jakarta.nosql.Entity;
import jakarta.nosql.Id;
import java.time.ZonedDateTime;

/**
 * @author Dmitry Repchevsky
 */

@Entity("Datasets")
public class DatasetEntity implements Dataset<DataUseConditionsEntity> {

    @Id("_id")
    private String _id;

    @Column("id")
    private String id;

    @Column("name")
    private String name;

    @Column("description")
    private String description;

    @Column("createDateTime")
    private ZonedDateTime createDateTime;
    
    @Column("updateDateTime")
    private ZonedDateTime updateDateTime;

    @Column("version")
    private String version;

    @Column("externalUrl")
    private String externalUrl;
    
    @Column("dataUseConditions")
    private DataUseConditionsEntity dataUseConditions;
    
    @Column("info")
    private JsonObject info;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    @Override
    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    @Override
    public ZonedDateTime getUpdateDateTime() {
        return updateDateTime;
    }

    @Override
    public void setUpdateDateTime(ZonedDateTime updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    @Override
    public String getVersion() {
        return version;
    }

    @Override
    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String getExternalUrl() {
        return externalUrl;
    }

    @Override
    public void setExternalUrl(String externalUrl) {
        this.externalUrl = externalUrl;
    }

    @Override
    public DataUseConditionsEntity getDataUseConditions() {
        return dataUseConditions;
    }

    @Override
    public void setDataUseConditions(DataUseConditionsEntity dataUseConditions) {
        this.dataUseConditions = dataUseConditions;
    }

    @Override
    public JsonObject getInfo() {
        return info;
    }

    @Override
    public void setInfo(JsonObject info) {
        this.info = info;
    }
    
}
