/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.nosql;

import es.bsc.inb.ga4gh.beacon.model.v200.variations.LegacyVariation;
import es.bsc.inb.ga4gh.vrs.model.jnosql.LocationInterface;
import jakarta.nosql.Column;

/**
 * @author Dmitry Repchevsky
 */

public class LegacyVariationEntity implements BeaconVariationEntity,
        LegacyVariation<LocationInterface> {

    @Column("alternateBases")
    private String alternateBases;
    
    @Column("location")
    private LocationInterface location;

    @Column("referenceBases")
    private String referenceBases;

    @Column("variantType")
    private String variantType;
    
    @Override
    public String getAlternateBases() {
        return alternateBases;
    }

    @Override
    public void setAlternateBases(String alternateBases) {
        this.alternateBases = alternateBases;
    }

    @Override
    public LocationInterface getLocation() {
        return location;
    }

    @Override
    public void setLocation(LocationInterface location) {
        this.location = location;
    }

    @Override
    public String getReferenceBases() {
        return referenceBases;
    }

    @Override
    public void setReferenceBases(String referenceBases) {
        this.referenceBases = referenceBases;
    }

    @Override
    public String getVariantType() {
        return variantType;
    }

    @Override
    public void setVariantType(String variantType) {
        this.variantType = variantType;
    }
    
}
