/**
 * *****************************************************************************
 * Copyright (C) 2024 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */
package es.bsc.inb.ga4gh.beacon.nosql.converter;

import es.bsc.inb.ga4gh.beacon.model.v200.variations.BeaconVariation;
import es.bsc.inb.ga4gh.beacon.nosql.BeaconVariationEntity;
import es.bsc.inb.ga4gh.beacon.nosql.value.JsonTypeReferenceReader;
import jakarta.json.JsonValue;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.json.bind.JsonbConfig;
import jakarta.json.bind.JsonbException;
import jakarta.nosql.AttributeConverter;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Dmitry Repchevsky
 */
public class BeaconVariationConverter
        implements AttributeConverter<BeaconVariation, Object> {

    private static final Logger LOGGER = Logger.getLogger(BeaconVariationConverter.class.getName());

    private static final Jsonb JSONB = JsonbBuilder.newBuilder().withConfig(new JsonbConfig()
                .withDeserializers(new BeaconVariationDeserializer())).build();

    @Override
    public Object convertToDatabaseColumn(BeaconVariation value) {
        if (value == null) {
            return null;
        }
        return JSONB.fromJson(JSONB.toJson(value), Map.class);
    }

    @Override
    public BeaconVariation convertToEntityAttribute(Object obj) {
        final JsonValue value = JsonTypeReferenceReader.convert(obj);
        if (value != null) {
            try {
                return JSONB.fromJson(value.toString(), BeaconVariationEntity.class);
            } catch (JsonbException ex) {
                LOGGER.log(Level.SEVERE, "error deserializing 'BeaconVariation' {0}", ex.getMessage());
            }
        }
        return null;
    }
    
}
