/**
 * *****************************************************************************
 * Copyright (C) 2024 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.nosql.converter;

import es.bsc.inb.ga4gh.beacon.nosql.BeaconVariationEntity;
import es.bsc.inb.ga4gh.beacon.nosql.LegacyVariationEntity;
import jakarta.json.JsonObject;
import jakarta.json.JsonValue;
import jakarta.json.bind.serializer.DeserializationContext;
import jakarta.json.bind.serializer.JsonbDeserializer;
import jakarta.json.stream.JsonParser;
import java.lang.reflect.Type;
import es.bsc.inb.ga4gh.beacon.nosql.VariationEntity;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;

/**
 * Custom Json deserializer which deserializes either to Beacon 'LegacyVariation' or
 * VRS Variations.
 * 
 * @author Dmitry Repchevsky
 */

public class BeaconVariationDeserializer implements JsonbDeserializer<BeaconVariationEntity> {

    private static final Jsonb JSONB = JsonbBuilder.create();
    
    @Override
    public BeaconVariationEntity deserialize(JsonParser parser, DeserializationContext ctx, Type type) {
        final JsonValue value = parser.getValue();
        if (JsonValue.ValueType.OBJECT == value.getValueType()) {
            final JsonObject obj = value.asJsonObject();
            final JsonValue obj_type = obj.get("type");
            if (obj_type == null) {
//                return ctx.deserialize(LegacyVariationEntity.class, 
//                        Json.createParserFactory(Collections.EMPTY_MAP)
//                                .createParser(obj));
                return JSONB.fromJson(value.toString(), LegacyVariationEntity.class);
            }
            if (JsonValue.ValueType.STRING == obj_type.getValueType()) {
//                return ctx.deserialize(VariationEntity.class,
//                        Json.createParserFactory(Collections.EMPTY_MAP)
//                                .createParser(obj));
                return JSONB.fromJson(value.toString(), VariationEntity.class);
            }
        }
        return null;
    }
}
