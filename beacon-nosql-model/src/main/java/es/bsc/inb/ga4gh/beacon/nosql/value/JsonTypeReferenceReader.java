/**
 * *****************************************************************************
 * Copyright (C) 2024 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.nosql.value;

import jakarta.json.Json;
import jakarta.json.JsonArrayBuilder;
import jakarta.json.JsonObject;
import jakarta.json.JsonObjectBuilder;
import jakarta.json.JsonValue;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.jnosql.communication.TypeReferenceReader;
import org.eclipse.jnosql.communication.TypeSupplier;
import org.eclipse.jnosql.communication.semistructured.Element;

/**
 * @author Dmitry Repchevsky
 */

public class JsonTypeReferenceReader implements TypeReferenceReader {

    @Override
    public boolean test(TypeSupplier<?> typeReference) {
        return typeReference.get() instanceof Class c &&
                JsonObject.class.isAssignableFrom(c);
    }

    @Override
    public <T> T convert(TypeSupplier<T> typeReference, Object obj) {
        return (T)convert(obj);
    }
    
    public static JsonValue convert(Object value) {
        if (value instanceof Element e) {
            return convert(e);
        } else if (value instanceof ArrayList l) {
            final JsonArrayBuilder builder = Json.createArrayBuilder();
            for (Object element : l) {
                builder.add(convert(element));
            }
            return builder.build();
        } else if (value instanceof List l) {
            final JsonObjectBuilder builder = Json.createObjectBuilder();
            for (Object property : l) {
                if (property instanceof Element e) {
                    builder.add(e.name(), convert(e));
                }
            }
            return builder.build();
        }

        return getJsonValue(value);
    }

    private static JsonValue convert(Element element) {
        return convert(element.get());
    }

    private static JsonValue getJsonValue(Object value) {
        if (value == null) {
            return JsonValue.NULL;
        } else if (value instanceof String) {
            return Json.createValue(value.toString());
        } else if (value instanceof Double) {
            return Json.createValue(Double.class.cast(value).doubleValue());
        } else if (value instanceof Long) {
            return Json.createValue(Long.class.cast(value).longValue());
        } else if (value instanceof Integer) {
            return Json.createValue(Integer.class.cast(value).intValue());
        } else if (value instanceof Boolean) {
            return Boolean.class.cast(value) ? JsonValue.TRUE : JsonValue.FALSE;
        }
        return null;
    }
}
