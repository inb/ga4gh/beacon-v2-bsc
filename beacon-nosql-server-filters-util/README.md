#### Auxiliary utility that extract ontological terms for configured filters.

It analyses /BEACON-INF/filtering_terms.json configuration file (imported from the **beacon-nosql-server-filters** module),
resoves found ontologies and finally generates the **beacon-filters-ontology.owl** ontology that contains a restricted set of terms
used by ontological filters.

Note that ontologies may be very big to download, it is much better to put them in the /BEACON-INF/ontologies/ directory before building the tool.
This way, if the ontology found in this directory, the tool is not going to use one defined in the "resources" part of filters configuration.

The tool usage:

    >java -jar generate-beacon-ontology.jar  
    processing ontology: thesaurus.owl.gz  
    => http://ncicb.nci.nih.gov/xml/owl/EVS/Thesaurus.owl# 9016746
    generating ontology: beacon-filters-ontology.owl  



