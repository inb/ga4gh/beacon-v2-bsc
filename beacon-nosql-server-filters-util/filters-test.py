#!/usr/bin/env python3

import sys

#
# The auxiliary toot to convert conventional boolean logic into the 'bracketless' form
#
# The 'bracketless' form follows the simple rule:
#
# No logic symbol means new 'and' group. This effectively closes previous group if exists.
#    "A B C D" == "(A) & (B) & (C) & (D)" = "A & B & C & D"
#    "A |B C |D &E" => "( A | B ) & ( C | D & E )"


def main():

    if (len(sys.argv) < 2):
        sys.exit("missting paran string")  
    
    p = sys.argv[1]
    if ('(' in p or ')' in p or '& ' in p or '| ' in p):
        print(encode(p))
    else:
        print(decode(p))


def encode(s):

    output = ''
    logic = '&'

    for param in s.split(' '):
        if (param == '|' or param == '&'):
            logic = param
        elif (param == ')'):
            logic = None
        elif (param != '('):
            if (len(output) > 0):
                output = output + ' '
            if (logic != None):
                output = output + logic
            output = output + param
        elif (logic == '&'):
            logic = None

    return output


def decode(s):

    output = None
    logic = '&'
    group = ''

    for param in s.split(' '):
        flogic = param[0] if param[0] == '|' or param[0] == '&' else None
        param = param if flogic == None else param[1:]

        if (flogic != None):
            if (len(group) == 0):
                output = param if output == None else output + ' ' + flogic + ' ' + param
            else:
                group = group + ' ' + flogic + ' ' + param
        else:
            if (len(group) > 0):
                # close group
                if (' ' in group):
                    group = '( ' + group + ' )'
                output = group if output == None else output + ' ' + logic + ' ' + group
            logic = '&'
            group = param
        print(group)
    if (len(group) > 0):
        if (output == None):
            output = group
        else:
            if (' ' in group):
                group = '( ' + group + ' )'
            output = output + ' ' + logic + ' ' + group        
  
    return output


if __name__ == "__main__":
    main()
