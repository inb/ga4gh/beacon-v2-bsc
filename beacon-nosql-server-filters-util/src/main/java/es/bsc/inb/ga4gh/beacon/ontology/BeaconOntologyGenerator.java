/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.ontology;

import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconFilteringTermsResponse;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconFilteringTermsResults;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.FilteringTerm;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.impl.DynamicModelFactory;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFWriter;
import org.eclipse.rdf4j.rio.Rio;

/**
 * @author Dmitry Repchevsky
 */

public final class BeaconOntologyGenerator {
    
    private final static String ONTOLOGIES_DIR = "/BEACON-INF/ontologies";
    
    public static void main(String[] args) {
        try {
            loadLocalOntologies();
            final Model model = generateSyntheticOntology();
            save(model);
        } catch (URISyntaxException | IOException ex) {
            Logger.getLogger(BeaconOntologyGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
   private static void loadLocalOntologies() throws URISyntaxException, IOException {
        final URL url = BeaconOntologyGenerator.class.getClassLoader().getResource(ONTOLOGIES_DIR.substring(1));
        if (url == null) {
            System.out.println(String.format("no preloaded ontologies found in %s", ONTOLOGIES_DIR));
        } else {
            final URI uri = url.toURI();
            final Path ontopath = "jar".equals(uri.getScheme()) 
                    ? FileSystems.newFileSystem(uri, Collections.EMPTY_MAP).getPath(ONTOLOGIES_DIR) 
                    : Paths.get(uri);

            final Iterator<Path> iter = Files.walk(ontopath, 1).iterator();
            while (iter.hasNext()) {
                final Path path = iter.next();
                if (Files.isRegularFile(path)) {
                    final Path filename = path.getFileName();
                    System.out.println(String.format("processing ontology: %s", filename));
                    try (InputStream in = Files.newInputStream(path, StandardOpenOption.READ);
                         GZIPInputStream gzip = new GZIPInputStream(in)) {
                        OntologiesRepository.instance().load(null, gzip);
                    } catch(Exception ex) {
                        Logger.getLogger(BeaconOntologyGenerator.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }
    
    private static Model generateSyntheticOntology() throws URISyntaxException, IOException {
        final Model model = new DynamicModelFactory().createEmptyModel();
        final OntologyFiltersProcessor processor = new OntologyFiltersProcessor();
        final BeaconFilteringTermsResponse response = processor.getFilteringTermsResponse();
        if (response != null) {
            final BeaconFilteringTermsResults results = response.getResponse();
            if (results != null) {
                final List<Resource> resources = results.getResources();
                if (resources != null) {
                    for (Resource resource : resources) {
                        final String url = resource.getUrl();
                        if (url != null) {
                            try {
                                OntologiesRepository.instance().load(resource.getIriPrefix(), url);
                            } catch (Exception ex) {
                                System.err.println(String.format("error loading ontology: %s", url));
                            }
                        }
                    }
                } 
            }
            final Map<String, ? extends FilteringTerm> filters = processor.getDefinedFilters(response);
            for (String filter : filters.keySet()) {
                final List<Statement> subfilters = OntologiesRepository.instance().getSubclasses(filter);
                model.addAll(subfilters);
            }
        }

        return model;
    }
    
    private static void save(Model model) {
        final URL url = BeaconOntologyGenerator.class.getProtectionDomain().getCodeSource().getLocation();
        try {
            final Path path = Paths.get(url.toURI().resolve(OntologyFiltersProcessor.DEFAULT_ONTOLOGY_FILE_NAME));
            System.out.println(String.format("generating ontology: %s", path.getFileName()));
            try (Writer out = Files.newBufferedWriter(path, StandardOpenOption.CREATE, StandardOpenOption.WRITE)) {
                final RDFWriter writer = Rio.createWriter(RDFFormat.RDFXML, out);
                writer.startRDF();
                for (Statement st: model) {
                    writer.handleStatement(st);
                }
                writer.endRDF();
            }
        } catch (Exception ex) {
            Logger.getLogger(BeaconOntologyGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
