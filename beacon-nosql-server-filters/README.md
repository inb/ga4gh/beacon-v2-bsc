#### Beacon filering_terms configuaration module.

The module includes filtering_terms.json configuration file and the code that axpands ontological filtering terms with 
descendant terms. It will use **/BEACON-INF/beacon-filters-ontology.owl** terms if found instead of going for the ontologies 
found in "resources" part of filters configuration.

![](/img/filters.png) "*generation of /filtering_terms*"

