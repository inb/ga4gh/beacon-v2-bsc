/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.jsonb;

import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.FilteringTerm;
import jakarta.json.bind.annotation.JsonbTransient;

/**
 * An extended FilteringTerm that includes 'query' property which is used by 
 * the filtering.
 * 
 * @author Dmitry Repchevsky
 */

public class BeaconFilteringTerm extends FilteringTerm {

    private String scope;
    private String namespace;
    private String query;

    public BeaconFilteringTerm() {}
    
    public BeaconFilteringTerm(BeaconFilteringTerm term) {
        super.setId(term.getId());
        super.setType(term.getType());
        super.setLabel(term.getLabel());
        super.setScopes(term.getScopes());

        this.scope = term.scope;
        this.namespace = term.namespace;
        this.query = term.query;
    }
    
    @JsonbTransient
    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    @JsonbTransient
    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }
    
    @JsonbTransient
    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
