/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.ontology;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;

/**
 * @author Dmitry Repchevsky
 */

public class OntologiesRepository {
    
    private final static OntologiesRepository repo = new OntologiesRepository();
    
    private final Map<String, Model> models;
    
    private OntologiesRepository() {
        models = new ConcurrentHashMap();
    }
    
    /**
     * Get the loaded model.
     * 
     * @param namespace the namespace of the model
     * 
     * @return the loaded model or null
     */
    public Model get(String namespace) {
        return models.get(namespace);
    }
    
    /**
     * Load an ontology into the repository.
     * 
     * @param namespace namespace of the loading ontology
     * @param url url location to load the ontology
     * 
     * @throws IOException 
     */
    public void load(String namespace, String url) throws IOException {
        if (models.containsKey(namespace)) {
            return; // already loaded
        }
        
        try (InputStream in = new URL(url).openStream()) {
            load(namespace, in);
        } catch (IOException ex) {
            Logger.getLogger(OntologiesRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void load(String namespace, InputStream in) throws IOException {
        final Model model = Rio.parse(in, RDFFormat.RDFXML);
        if (namespace == null) {
            final Set<Namespace> namespaces = model.getNamespaces();
            for (Namespace nmsp : namespaces) {
                final String pfx = nmsp.getPrefix();
                if (pfx == null || pfx.isEmpty()) {
                    namespace = nmsp.getName();
                    if (namespace != null) {
                        break;
                    }
                }
            }
        }
        Logger.getAnonymousLogger().log(Level.FINEST, "add ontology: {0}", namespace);
        System.out.println("=> " + namespace + " " + model.size());
        models.put(namespace, model);
    }
    
    /**
     * Return RDFS.LABEL statements for all subclasses of the @param clazz
     * 
     * @param iri IRI class identifier
     *            (e.g. 'http://ncicb.nci.nih.gov/xml/owl/EVS/Thesaurus.owl#:NCIT:C20197')
     * 
     * @return a list of all @param iri subclasses
     */
    public List<Statement> getFilters(String iri) {
        final List<Statement> statements = getSubclasses(iri);
        final Stream<Statement> stream = statements.stream();
        return stream.filter(p -> p.getPredicate().equals(RDFS.LABEL))
                     .collect(Collectors.toList());
    }

    /**
     * Return statements of all class subclasses ('rdfs:subClassOf')
     * and their correspondent labels ('rdfls:label').
     * 
     * @param clazz the class for which subclasses are queried
     * 
     * @return the list of all subclasses and their labels
     */
    public List<Statement> getSubclasses(String clazz) {
        final List<Statement> list = new ArrayList();
        final IRI iri = SimpleValueFactory.getInstance().createIRI(clazz);
        
        final Model model = models.get(iri.getNamespace());
        for (Model m : model == null ? models.values() : List.of(model)) {
            final Statement parent = SimpleValueFactory.getInstance().createStatement(iri, RDF.TYPE, RDFS.CLASS);
            for(Statement label : m.getStatements(parent.getSubject(), RDFS.LABEL, null)) {
                list.add(label);
            }
            collectSubclasses(m, parent, list);
        }
        return list;
    }
    
    public void collectSubclasses(Model model, Statement parent, List<Statement> list) {
        final Resource subject = parent.getSubject();
        if (subject instanceof IRI) {
            final IRI iri = (IRI)subject;
            for(Statement statement : model.getStatements(null, RDFS.SUBCLASSOF, iri)) {
                list.add(statement);
                for(Statement label : model.getStatements(statement.getSubject(), RDFS.LABEL, null)) {
                    list.add(label);
                }
                collectSubclasses(model, statement, list);
            }
        }
    }
    
    public static OntologiesRepository instance() {
        return repo;
    }
}
