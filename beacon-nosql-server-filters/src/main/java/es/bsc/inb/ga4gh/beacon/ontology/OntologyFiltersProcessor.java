/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.ontology;

import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconFilteringTermsResponse;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconFilteringTermsResults;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.FilteringTerm;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.Resource;
import es.bsc.inb.ga4gh.beacon.jsonb.BeaconFilteringTerm;
import es.bsc.inb.ga4gh.beacon.jsonb.BeaconFilteringTermDeserializer;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.json.bind.JsonbConfig;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

/**
 * 
 * 
 * @author Dmitry Repchevsky
 */

public class OntologyFiltersProcessor {
    
    public final static String FILTERING_TERMS_FILE = "BEACON-INF/filtering_terms.json";
    public final static String DEFAULT_ONTOLOGY_FILE_NAME = "beacon-filters-ontology.owl";
    
    private final static Jsonb JSONB = JsonbBuilder.create(new JsonbConfig()
            .withDeserializers(new BeaconFilteringTermDeserializer()));
    
    private BeaconFilteringTermsResponse filterin_terms_response;
    
    public OntologyFiltersProcessor() {
        loadDefaultOntology();
        loadFilteringTermsResponse();
    }

    /**
     * Create the 'expanded' filtering terms response for the scope
     * 
     * @param entry_type the scope (e.g. 'individual') to filter the filters or null.
     * 
     * @return filtering terms response object
     */
    public BeaconFilteringTermsResponse getExpandedFilteringTermsResponse(String entry_type) {
        if (filterin_terms_response != null) {
            final BeaconFilteringTermsResponse response = new BeaconFilteringTermsResponse();
            response.setMeta(filterin_terms_response.getMeta());
            BeaconFilteringTermsResults results = filterin_terms_response.getResponse();
            if (results != null) {
                final List<Resource> resources = results.getResources();
                final List<FilteringTerm> filtering_terms = results.getFilteringTerms();
                
                response.setResponse(results = new BeaconFilteringTermsResults());
                results.setResources(resources);
                
                if (filtering_terms != null) {
                    response.setResponse(results);
                    final Map<String, FilteringTerm> filters = new HashMap();
                    for (FilteringTerm filtering_term : filtering_terms) {
                        final String id = filtering_term.getId();
                        final String type = filtering_term.getType();
                        if (id != null && type != null) {
                            final BeaconFilteringTerm beacon_filtering_term = (BeaconFilteringTerm)filtering_term;
                            final String scope = beacon_filtering_term.getScope();
                            if (entry_type == null || entry_type.equals(scope)) {
                                FilteringTerm filter = filters.get(type + ":" + id);
                                if (filter == null) {
                                    filter = new BeaconFilteringTerm(beacon_filtering_term);
                                    filters.put(type + ":" + id, filter);
                                }
                                if (scope != null) {
                                    List<String> scopes = filter.getScopes();
                                    if (scopes == null) {
                                        filter.setScopes(scopes = new ArrayList());
                                    }
                                    scopes.add(scope);
                                }
                            }
                        }
                    }
                    results.setFilteringTerms(new ArrayList(filters.values()));

                    final Set<BeaconFilteringTerm> expanded_filters = getExpandedFilters(response);
                    if (!expanded_filters.isEmpty()) {
                        results.getFilteringTerms().addAll(expanded_filters);
                    }
                }
                
                
            }
            return response;
        }
        return null;
    }

//    /**
//     * Get ontology filters defined in the configuration file for a given endpoint.
//     *
//     * @return the list of resolved filter IRIs 
//     * (e.g. 'NCIT:C16576' -> 'http://ncicb.nci.nih.gov/xml/owl/EVS/Thesaurus.owl#C16576')
//     */
//    public Map<String, ? extends FilteringTerm> getDefinedFilters() {
//        final BeaconFilteringTermsResponse response = getFilteringTermsResponse();
//        return response != null ? getDefinedFilters(response) : Collections.EMPTY_MAP;
//    }

    public Map<String, ? extends FilteringTerm> getDefinedFilters(BeaconFilteringTermsResponse response) {
        final Map<String, FilteringTerm> filters = new HashMap();

        final BeaconFilteringTermsResults results = response.getResponse();
        final List<Resource> resources = results.getResources();

        // there won't be any changes if no ontologies are defined!
        if (resources != null) {
            final List<FilteringTerm> terms = results.getFilteringTerms();
            for (FilteringTerm term : terms) {
                final String type = term.getType();
                if ("ontology".equals(type)) {
                    final String id = term.getId();
                    if (id == null) {
                        Logger.getLogger(OntologyFiltersProcessor.class.getName()).log(
                            Level.SEVERE, "missed id for the ontology term");
                    } else {
                        final int idx = id.indexOf(':');
                        if (idx > 0) {
                            final String prefix = id.substring(0, idx);
                            if (prefix != null) {
                                for (Resource resource : resources) {
                                    if (prefix.equals(resource.getNameSpacePrefix())) {
                                        final String namespace = resource.getIriPrefix();

                                        // skip loading ontologies if there is a default predefined one
                                        if (OntologiesRepository.instance().get(DEFAULT_ONTOLOGY_FILE_NAME) == null) {
                                            final String url = resource.getUrl();
                                            try {
                                                OntologiesRepository.instance().load(namespace, url);
                                            } catch(IOException ex) {
                                                Logger.getLogger(OntologyFiltersProcessor.class.getName()).log(
                                                        Level.SEVERE, "error loading ontology: {0}", url);
                                            }
                                        }

                                        ((BeaconFilteringTerm)term).setNamespace(namespace);
                                        filters.put(namespace + id.substring(idx + 1), term);
                                        break; // found
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return filters;
    }

    public Set<BeaconFilteringTerm> getExpandedFilters(BeaconFilteringTermsResponse response) {
        final Set<BeaconFilteringTerm> filters = new HashSet();
        for (Map.Entry<String, ? extends FilteringTerm> entry : getDefinedFilters(response).entrySet()) {
            final String clazz = entry.getKey();
            final String namespace = SimpleValueFactory.getInstance().createIRI(clazz).getNamespace();
            final BeaconFilteringTerm root_term = (BeaconFilteringTerm)entry.getValue();
            final String id = root_term.getId();
            if (id == null) {
                Logger.getLogger(OntologyFiltersProcessor.class.getName()).log(
                    Level.SEVERE, "missed id for the ontology term");
            } else {
                final int idx = id.indexOf(':');
                final String prefix = idx > 0 ? id.substring(0, idx) : null;
                
                final List<Statement> subfilters = OntologiesRepository.instance()
                                .getFilters(clazz);
                for (Statement subfilter : subfilters) {
                    if (subfilter.getSubject().isIRI()) {
                        final BeaconFilteringTerm term = new BeaconFilteringTerm(root_term);
                        final IRI iri = (IRI)subfilter.getSubject();
                        final String name = iri.getLocalName();
                        final String nmsp = iri.getNamespace();
                        if (prefix != null && namespace.equals(nmsp)) {
                            term.setId(prefix + ':' + name);
                        } else {
                            final BeaconFilteringTermsResults results = response.getResponse();
                            final List<Resource> resources = results.getResources();
                            for (Resource resource : resources) {
                                if (namespace.equals(resource.getIriPrefix())) {
                                    final String pfx = resource.getNameSpacePrefix();
                                    if (pfx != null && !pfx.isEmpty()) {
                                        term.setId(pfx + ':' + name);
                                        break;
                                    }
                                }
                            }
                            if (term.getId() == null) {
                                continue; // unknown ontology, skip
                            }
                        }
                        term.setLabel(subfilter.getObject().stringValue());
                        term.setNamespace(iri.getNamespace());
                        filters.add(term);
                    }
                }
            }
        }
        return filters;
    }
    
    /**
     * Get all descendants of the filter from the ontologies.
     * 
     * @param id class identifier (e.g. 'C16576')
     * @param namespace class namespace (e.g. 'http://ncicb.nci.nih.gov/xml/owl/EVS/Thesaurus.owl#')
     * 
     * @return a list of descendants´ IRIs
     */
    public List<String> getDescendants(String id, String namespace) {
        if (id == null) {
            Logger.getLogger(OntologyFiltersProcessor.class.getName()).log(
                Level.INFO, "missed filter id");
        } else if (namespace == null) {
            Logger.getLogger(OntologyFiltersProcessor.class.getName()).log(
                Level.INFO, "missed filter {0} namespace", id);
        } else {
            final int idx = id.indexOf(':');
            final String iri = namespace + id.substring(Math.max(0, idx + 1));
            final List<Statement> statements = 
                    OntologiesRepository.instance().getFilters(iri);
            return statements.stream().map(Statement::getSubject)
                    .filter(p -> p.isIRI())
                    .map(IRI.class::cast)
                    .map(IRI::toString)
                    .collect(Collectors.toList());
        }
        return Collections.EMPTY_LIST;
    }
    
    public BeaconFilteringTermsResponse getFilteringTermsResponse() {
        return filterin_terms_response;
    }
    
    private void loadDefaultOntology() {
        final String file = "/BEACON-INF/" + DEFAULT_ONTOLOGY_FILE_NAME;
        try (InputStream in = OntologyFiltersProcessor.class.getClassLoader().getResourceAsStream(file)) {
            if (in == null) {
                Logger.getLogger(OntologyFiltersProcessor.class.getName()).log(
                        Level.FINEST, "no default filtering terms file found: {0}", DEFAULT_ONTOLOGY_FILE_NAME);
            } else {
                OntologiesRepository.instance().load(DEFAULT_ONTOLOGY_FILE_NAME, in);
            }
        } catch (Exception ex) {
            Logger.getLogger(OntologyFiltersProcessor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void loadFilteringTermsResponse() {
        try (InputStream in = OntologyFiltersProcessor.class.getClassLoader().getResourceAsStream(FILTERING_TERMS_FILE)) {
            if (in == null) {
                Logger.getLogger(OntologyFiltersProcessor.class.getName()).log(
                        Level.SEVERE, "no filtering terms file found: {0}", FILTERING_TERMS_FILE);
            } else {
                filterin_terms_response = JSONB.fromJson(in, BeaconFilteringTermsResponse.class);
            }
        } catch (Exception ex) {
            Logger.getLogger(OntologyFiltersProcessor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
