/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.application;

import es.bsc.inb.ga4gh.beacon.framework.model.v200.configuration.BeaconMap;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.configuration.Endpoint;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconFilteringTermsResponse;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconMapResponse;
import es.bsc.inb.ga4gh.beacon.ontology.OntologyFiltersProcessor;
import es.bsc.inb.ga4gh.beacon.rest.ext.URIInfoProducer;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Produces;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Dmitry Repchevsky
 */

@ApplicationScoped
public class BeaconFilteringTermsProducer {

    @Inject
    private BeaconMapResponse map_response;
    
    @Inject
    private URIInfoProducer uriInfoProducer;
    
    @Inject
    private OntologyFiltersProcessor processor;
    
    private Map<String, String> filtering_terms_scopes;

    /**
     * The map of filtering terms responses the entity types.
     * (e.g. Map&lt;'biosample', obj&gt;)
     */
    private Map<String, BeaconFilteringTermsResponse> filtering_terms;

    /**
     * Automatically generated global filtering terms response.
     * The response is an aggregation of all filtering terms 
     * defined by the configuration.
     */
    private BeaconFilteringTermsResponse filtering_terms_response;
    
    @PostConstruct
    public void init() {
        filtering_terms_scopes = new ConcurrentHashMap();
        filtering_terms = new ConcurrentHashMap();
        
        final BeaconMap beacon_map = map_response.getResponse();
        if (beacon_map != null) {
            final Map<String, Endpoint> endpoints_map = beacon_map.getEndpointSets();
            if (endpoints_map != null) {
                for (Endpoint endpoint : endpoints_map.values()) {
                    final String entry_type = endpoint.getEntryType();
                    if (entry_type != null) {
                        final BeaconFilteringTermsResponse response = processor.getExpandedFilteringTermsResponse(entry_type);
                        if (response != null) {
                            filtering_terms.put(entry_type, response);
                            final String filtering_terms_url = endpoint.getFilteringTermsUrl();
                            if(filtering_terms_url != null) {
                                try {
                                    final URI filtering_terms_uri = URI.create(filtering_terms_url);
                                    filtering_terms_scopes.put(filtering_terms_uri.getPath(), entry_type);
                                } catch(IllegalArgumentException ex) {
                                    Logger.getLogger(BeaconFilteringTermsProducer.class.getName()).log(
                                            Level.SEVERE, "invalid filteringTermsUrl: {0}", filtering_terms_url);
                                }
                            }
                        }
                    }
                }
            }
        }
        
        filtering_terms_response = processor.getExpandedFilteringTermsResponse(null);
    }
    
    public String getFilteringTermsScope() {
        final UriInfo uriInfo = uriInfoProducer.getUriInfo();
        return filtering_terms_scopes.get(uriInfo.getAbsolutePath().getPath().replaceFirst("/$", ""));
    }

    public BeaconFilteringTermsResponse getFilteringTerms() {
        final String scope = getFilteringTermsScope();
        if (scope != null) {
            final BeaconFilteringTermsResponse response = filtering_terms.get(scope);
            if (response != null) {
                return response;
            }
        }
        return new BeaconFilteringTermsResponse();
    }

    public Map<String, BeaconFilteringTermsResponse> getFilteringTermsResponses() {
        return filtering_terms;
    }
    
    @Produces
    public BeaconFilteringTermsResponse getFilteringTermsResponse() {
        return filtering_terms_response;
    }
}
