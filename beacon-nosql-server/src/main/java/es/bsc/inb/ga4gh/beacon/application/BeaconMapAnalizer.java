/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.application;

import es.bsc.inb.ga4gh.beacon.framework.model.v200.configuration.BeaconMap;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.configuration.Endpoint;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.configuration.RelatedEndpoint;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconMapResponse;
import es.bsc.inb.ga4gh.beacon.rest.ext.URIInfoProducer;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.util.Map;

/**
 * @author Dmitry Repchevsky
 */

@ApplicationScoped
public class BeaconMapAnalizer {

    @Inject
    private URIInfoProducer uriInfoProducer;

    @Inject
    private BeaconMapResponse map_response;

    /**
     * Return current request 'entryType'.
     * For instance '/individuals/123/biosamples' returns 'Biosamples'
     * 
     * @return entryType name associated with current request
     */
    public String getCurrentEntryType() {
        final BeaconMap beacon_map = map_response.getResponse();
        if (beacon_map != null) {
            final Map<String, Endpoint> endpoints_map = beacon_map.getEndpointSets();
            if (endpoints_map != null) {
                final String path = uriInfoProducer.getUriInfo().getAbsolutePath().toString();
                for (Endpoint endpoint : endpoints_map.values()) {
                    final String entry_type = endpoint.getEntryType();
                    if (entry_type != null) {
                        final String root_url = endpoint.getRootUrl();
                        if (match(path, root_url)) {
                            return entry_type;
                        }
                        
                        final String single_entry_url = endpoint.getSingleEntryUrl();
                        if (match(path, single_entry_url)) {
                            return entry_type;
                        }
                        
                        final Map<String, RelatedEndpoint> related_endpoints = endpoint.getEndpoints();
                        if (related_endpoints != null) {
                            for (RelatedEndpoint related_endpont : related_endpoints.values()) {
                                final String type = related_endpont.getReturnedEntryType();
                                final String related_url = related_endpont.getUrl();
                                if (match(path, related_url)) {
                                    return type;
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    }
    
    public String getEndpointName() {
        final BeaconMap beacon_map = map_response.getResponse();
        if (beacon_map != null) {
            final Map<String, Endpoint> endpoints_map = beacon_map.getEndpointSets();
            if (endpoints_map != null) {
                final String path = uriInfoProducer.getUriInfo().getAbsolutePath().toString();
                for (Map.Entry<String, Endpoint> entry : endpoints_map.entrySet()) {
                    final String endpoint_name = entry.getKey();
                    final Endpoint endpoint = entry.getValue();
                    final String entry_type = endpoint.getEntryType();
                    if (entry_type != null) {
                        final String root_url = endpoint.getRootUrl();
                        if (match(path, root_url)) {
                            return endpoint_name;
                        }
                        
                        final String single_entry_url = endpoint.getSingleEntryUrl();
                        if (match(path, single_entry_url)) {
                            return endpoint_name;
                        }
                        
                        final Map<String, RelatedEndpoint> related_endpoints = endpoint.getEndpoints();
                        if (related_endpoints != null) {
                            for (RelatedEndpoint related_endpont : related_endpoints.values()) {
                                final String type = related_endpont.getReturnedEntryType();
                                final String related_url = related_endpont.getUrl();
                                if (match(path, related_url)) {
                                    return endpoint_name;
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;        
    }

    /**
     * Find endpoint name for the provided entryType.
     * Example: getEndpointName("genomicVariant") => "Variants"
     * 
     * @param entry_type entryType for which the endpoint name is searched
     * 
     * @return the endpoint name or null
     */
    public String getEndpointName(String entry_type) {
        if (entry_type != null) {
            final BeaconMap beacon_map = map_response.getResponse();
            if (beacon_map != null) {
                final Map<String, Endpoint> endpoints_map = beacon_map.getEndpointSets();
                if (endpoints_map != null) {
                    for (Map.Entry<String, Endpoint> entry : endpoints_map.entrySet()) {
                        if (entry_type.equals(entry.getValue().getEntryType())) {
                            return entry.getKey();
                        }
                    }
                }
            }
        }
        return null;
    }
    
    /**
     * Check if the request path matches some endpoint template.
     * 
     * @param path the request path to test
     * @param endpoint the template to test
     * 
     * @return true if matches
     */
    private boolean match(String path, String endpoint) {
        if (endpoint != null) {
            final String[] names1 = path.split("/");
            final String[] names2 = endpoint.split("/");
            if (names1.length == names2.length) {
                for (int i = 0, n = names2.length; i < n; i++) {
                    if ((names2[i].startsWith("{") && names2[i].endsWith("}")) || 
                         names1[i].equals(names2[i])) {
                        continue;
                    }
                    return false;
                }
                return true;
            }
        }
        return false;
    }

}
