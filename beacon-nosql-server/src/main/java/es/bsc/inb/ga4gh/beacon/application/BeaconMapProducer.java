/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.application;

import es.bsc.inb.ga4gh.beacon.config.BeaconOpenAPIProducer;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.configuration.BeaconMap;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.configuration.Endpoint;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.configuration.RelatedEndpoint;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconMapResponse;
import es.bsc.inb.ga4gh.beacon.rest.ext.URIInfoProducer;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Produces;
import jakarta.inject.Inject;
import jakarta.json.bind.JsonbBuilder;
import jakarta.servlet.ServletContext;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Dmitry Repchevsky
 */

@ApplicationScoped
public class BeaconMapProducer {

    private final static String BEACON_MAP_FILE = "BEACON-INF/beacon_map.json";
    
    @Inject 
    private ServletContext ctx;

    @Inject
    private URIInfoProducer uriInfoProducer;
    
    @Inject
    private BeaconOpenAPIProducer openAPIProducer;

    private BeaconMapResponse map_response;
    
    @PostConstruct
    public void init() {
        try (InputStream in = ctx.getResourceAsStream(BEACON_MAP_FILE)) {
            if (in == null) {
                Logger.getLogger(BeaconMapProducer.class.getName()).log(
                        Level.SEVERE, "no beacons map file found: " + BEACON_MAP_FILE);
            } else {
                map_response = JsonbBuilder.create().fromJson(in, BeaconMapResponse.class);
                fixBeaconMap(map_response.getResponse());
            }
        } catch (IOException ex) {
            Logger.getLogger(BeaconMapProducer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Produces
    public BeaconMapResponse beaconMap() {
        return map_response;
    }
    
    /**
     * Generates absolute URLs for the BeaconMap.
     * 
     * The beacon_map.json configuration provides relative URLs for the endpoints. 
     * This method generates the absolute URLs based on the host the server is run on.
     * 
     * @param beacon_map the BeaconMap configuration
     */
    private void fixBeaconMap(BeaconMap beacon_map) {
        if (beacon_map != null) {
            final Map<String, Endpoint> endpoints_map = beacon_map.getEndpointSets();
            if (endpoints_map != null) {
                final URI base_uri = uriInfoProducer.getUriInfo().getBaseUri();
                for (Endpoint endpoint : endpoints_map.values()) {
                    final String entry_type = endpoint.getEntryType();
                    if (entry_type != null) {
                        final String root_url = resolve(base_uri, endpoint.getRootUrl());
                        endpoint.setRootUrl(root_url);
                        
                        openAPIProducer.fixOpenAPIEndpoint(endpoint);
                        
                        final String filtering_terms_url = resolve(
                                root_url != null ? URI.create(root_url) : base_uri,
                                endpoint.getFilteringTermsUrl());
                        endpoint.setFilteringTermsUrl(filtering_terms_url);
                        
                        final String single_entry_url = resolve(
                                root_url != null ? URI.create(root_url) : base_uri,
                                endpoint.getSingleEntryUrl());
                        endpoint.setSingleEntryUrl(single_entry_url);
                        
                        final Map<String, RelatedEndpoint> related_endpoints = endpoint.getEndpoints();
                        if (related_endpoints != null) {
                            for (RelatedEndpoint related_endpont : related_endpoints.values()) {
                                final String related_url = resolve(
                                        root_url != null ? URI.create(root_url) : base_uri,
                                        related_endpont.getUrl());
                                related_endpont.setUrl(related_url);
                            }
                        }
                    }
                }
            }
        }
    }
    
    private String resolve(URI base, String template) {
        if (template == null) {
            return null;
        }
        
        final String encoded = template.replaceAll("\\{", "%7B").replaceAll("\\}", "%7D");
            
        final String base_path = base.getPath();
        try {
            URI uri = new URI(encoded);
            if (uri.isAbsolute()) {
                return template;
            }

            if (template.startsWith("/")) {
                // safe check if relative url template includes '/beacon/v2.0.0/' part
                if (template.startsWith(base_path)) {
                    uri = new URI(base.getScheme(), base.getUserInfo(),
                        base.getHost(), base.getPort(), template, null, null);
                } else {
                    uri = base.resolve(encoded.substring(1));
                }
            } else if (!base_path.regionMatches(1, template, 0, base_path.length() - 1)) {
                uri = base.resolve(uri);
            } else {
                uri = new URI(base.getScheme(), base.getUserInfo(),
                    base.getHost(), base.getPort(), "/"  + template, null, null);
            }
            return URLDecoder.decode(uri.toString(), StandardCharsets.UTF_8);
        } catch(URISyntaxException ex) {
            Logger.getLogger(BeaconFilteringTermsProducer.class.getName()).log(
                    Level.SEVERE, "invalid url: {0}", ex.getInput());
        }
        return null;
    }
}
