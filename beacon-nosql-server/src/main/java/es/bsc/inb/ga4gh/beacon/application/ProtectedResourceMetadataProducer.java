/**
 * *****************************************************************************
 * Copyright (C) 2024 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.application;

import es.bsc.inb.ga4gh.beacon.config.BeaconOpenAPIProducer;
import es.bsc.inb.ga4gh.beacon.rest.ext.URIInfoProducer;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.json.JsonValue;
import java.util.Arrays;

/**
 * @author Dmitry Repchevsky
 */

public class ProtectedResourceMetadataProducer {
    
    public final static String OPENID_CONFIGURATION_URI = ".well-known/openid-configuration";

    @Inject
    private URIInfoProducer uriInfoProducer;
    
    @Inject
    private BeaconOpenAPIProducer openapiProducer;
    
    private JsonObject metadata;
    
    @PostConstruct
    public void init() {
        final String openIdConnectUrl = getOpenIdConnectUrl();
        if (openIdConnectUrl != null && openIdConnectUrl.endsWith(OPENID_CONFIGURATION_URI)) {
            final String clientId = getOpenIdConnectClientId();
            final String resource = uriInfoProducer.getUriInfo().getBaseUri().toString();
            metadata = Json.createObjectBuilder()
                    .add("resource", resource)
                    .add("authorization_servers", Json.createArrayBuilder(
                            Arrays.asList(openIdConnectUrl.substring(0, 
                                    openIdConnectUrl.length() - OPENID_CONFIGURATION_URI.length() - 1))))
                    .add("client_id", clientId != null ? Json.createValue(clientId): JsonValue.NULL) 
                    .build();
        }
    }
    
    private String getOpenIdConnectUrl() {
        final JsonObject schema = getOpenIdConnectSchema();
        if (schema != null) {
            final JsonValue url = schema.get("openIdConnectUrl");
            if (url != null && JsonValue.ValueType.STRING == url.getValueType()) {
                return schema.asJsonObject().getString("openIdConnectUrl");
            }
        }
        return null;
    }

    private String getOpenIdConnectClientId() {
        final JsonObject schema = getOpenIdConnectSchema();
        if (schema != null) {
            final JsonValue url = schema.get("x-client-id");
            if (url != null && JsonValue.ValueType.STRING == url.getValueType()) {
                return schema.asJsonObject().getString("x-client-id");
            }
        }
        return null;
    }

    private JsonObject getOpenIdConnectSchema() {
        final JsonObject openapi = openapiProducer.getOpenAPI();
        if (openapi != null) {
            final JsonValue components = openapi.get("components");
            if (components != null && JsonValue.ValueType.OBJECT == components.getValueType()) {
                final JsonValue schemes = components.asJsonObject().get("securitySchemes");
                if (schemes != null && JsonValue.ValueType.OBJECT == schemes.getValueType() &&
                    !schemes.asJsonObject().values().isEmpty()) {
                    final JsonValue schema = schemes.asJsonObject().values().iterator().next();
                    if (schema != null && JsonValue.ValueType.OBJECT == schema.getValueType()) {
                        return schema.asJsonObject();
                    }
                }
            }
        }
        return null;
    }
    
    public JsonObject getMetadata() {
        return metadata;
    }
}
