/**
 * *****************************************************************************
 * Copyright (C) 2024 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.config;

import es.bsc.inb.ga4gh.beacon.application.BeaconMapProducer;
import es.bsc.inb.ga4gh.beacon.application.ServiceConfigurationProducer;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.configuration.Endpoint;
import es.bsc.inb.ga4gh.beacon.rest.ext.URIInfoProducer;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.json.Json;
import jakarta.json.JsonMergePatch;
import jakarta.json.JsonObject;
import jakarta.json.JsonObjectBuilder;
import jakarta.servlet.ServletContext;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Dmitry Repchevsky
 */

@ApplicationScoped
public class BeaconOpenAPIProducer {
    
    private final static String BEACON_OPENAPI_FILE = "BEACON-INF/openapi.json";
 
    @Inject 
    private ServletContext ctx;

    @Inject
    private URIInfoProducer uriInfoProducer;

    @Inject
    private ServiceConfigurationProducer configurationProducer;
    
    @Inject
    private OpenAPILoader loader;

    private JsonObject openapi_config;
    private final Map<String, JsonObject> openapis;
    
    public BeaconOpenAPIProducer() {
        openapis = new ConcurrentHashMap();
    }
    
    @PostConstruct
    public void init() {
        try (InputStream in = ctx.getResourceAsStream(BEACON_OPENAPI_FILE)) {
            if (in == null) {
                Logger.getLogger(BeaconMapProducer.class.getName()).log(
                        Level.INFO, "no beacons openapi file found: " + BEACON_OPENAPI_FILE);
            } else {
                openapi_config = Json.createReader(in).readObject();
            }
        } catch (IOException ex) {
            Logger.getLogger(BeaconMapProducer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public JsonObject getOpenAPI() {
        return openapi_config;
    }
    
    public JsonObject getOpenAPI(String endpoint) {
        return openapis.get(endpoint);
    }
    
    public void fixOpenAPIEndpoint(Endpoint endpoint) {
        final JsonObject openapi_gen = generateOpenAPIEndpoint(endpoint);
        final String openapi_url = endpoint.getOpenAPIEndpointsDefinition();
        if (openapi_url != null) {
            try {
                final JsonObject openapi = loader.getOpenAPI(
                        URI.create(openapi_url));
                if (openapi != null) {
                    final JsonObject fixed_openapi = mergeOpenAPI(openapi_gen, openapi);
                    fixOpenAPIEndpoint(endpoint, fixed_openapi);
                    return;
                }
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(BeaconOpenAPIProducer.class.getName()).log(Level.INFO, null, ex);
            }
        }

        // no loaded openapi endpoint
        fixOpenAPIEndpoint(endpoint, openapi_gen);
    }
    
    /**
     * Generates OpenAPI endpoint from beacon's /map endpoint definition
     * 
     * @param endpoint
     * @return 
     */
    private JsonObject generateOpenAPIEndpoint(Endpoint endpoint) {
        final String base = uriInfoProducer.getUriInfo().getBaseUri()
                .toString().replaceAll("/$", "");
        
        final JsonObjectBuilder openapi = 
                openapi_config != null ? Json.createObjectBuilder(openapi_config) 
                : Json.createObjectBuilder();

        openapi.add("servers", Json.createArrayBuilder()
                .add(Json.createObjectBuilder(Map.of("url", base))));
        
        final int base_length = base.length();
        
        final JsonObjectBuilder paths = Json.createObjectBuilder();
        
        addEndpoint(base_length, endpoint, paths);
        
        openapi.add("paths", paths);
        
        return openapi.build();
    }
    
    private void addEndpoint(int base_length, Endpoint endpoint, JsonObjectBuilder paths) {
        final JsonObjectBuilder root = Json.createObjectBuilder();
        final String root_url = endpoint.getRootUrl();
        addPost(root, endpoint.getEntryType());
        
        paths.add(root_url.substring(base_length), root);

    }

    private void addPost(JsonObjectBuilder path, String type) {
        final JsonObjectBuilder post = Json.createObjectBuilder();
        addRequestBody(post, type);
        addResponses(post, type);
        path.add("post", post);
    }

    private void addRequestBody(JsonObjectBuilder post, String type) {
        final JsonObjectBuilder requestBody = Json.createObjectBuilder();
        final JsonObjectBuilder content = Json.createObjectBuilder();
        final JsonObjectBuilder json = Json.createObjectBuilder();
        final JsonObjectBuilder schema = Json.createObjectBuilder();
        
        final String entryTypeSchema = configurationProducer.getEntryTypeSchema(type);
        schema.add("$ref", entryTypeSchema);
        
        json.add("schema", schema);
        content.add("application/json", json);
        requestBody.add("content", content);
        requestBody.add("required", true);
        post.add("requestBody", requestBody);
    }

    private void addResponses(JsonObjectBuilder post, String type) {
        final JsonObjectBuilder responses = Json.createObjectBuilder();
        final JsonObjectBuilder _200 = Json.createObjectBuilder();
        final JsonObjectBuilder _default = Json.createObjectBuilder();
        
        // TODO: nothing for now (no way to get schemas from /configuration)
        
        responses.add("200", _200);
        responses.add("default", _default);
        post.add("resonses", responses);
    }
    
    private void fixOpenAPIEndpoint(Endpoint endpoint, JsonObject fixed_openapi) {
        final String root_url = endpoint.getRootUrl();
        final String openapi_endpoint = 
                root_url.replaceAll("/$", "") + "-openapi.json";
        endpoint.setOpenAPIEndpointsDefinition(openapi_endpoint);
        openapis.put(openapi_endpoint, fixed_openapi);
    }
            
    private JsonObject mergeOpenAPI(JsonObject openapi_gen, JsonObject openapi) {
        final JsonMergePatch patch = Json.createMergePatch(openapi_gen);
        return patch.apply(openapi).asJsonObject();
    }
}
