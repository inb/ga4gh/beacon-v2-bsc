/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.config;

import es.bsc.inb.ga4gh.beacon.application.ServiceConfigurationProducer;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.common.SchemaPerEntity;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.common.SchemaReference;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.configuration.BeaconConfiguration;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.configuration.BeaconMap;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.configuration.BeaconSecurityAttributes;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.configuration.Endpoint;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.configuration.RelatedEndpoint;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.configuration.ServiceConfiguration;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconInformationalResponseMeta;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconMapResponse;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.EntryTypeDefinition;
import es.bsc.inb.ga4gh.beacon.rest.ext.URIInfoProducer;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.inject.Produces;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Produces {@link DefaultResponseMetadata} on 
 * the basement of the caller's request path and mapping configuration found in
 * the beacon_map.json file.
 * 
 * @author Dmitry Repchevsky
 */

@Singleton
public class ResponseMetadataProducer {

    @Inject
    private ServiceConfigurationProducer configurationProducer;

    @Inject
    private BeaconMapResponse mapResponse;
    
    @Inject
    private URIInfoProducer uriInfoProducer;
    
    private String beaconId;
    private String apiVersion;
    private String defaultGranularity;
    private Integer maxPageSize;

//    private String entryType;
//    private SchemaPerEntity defaultSchema;
        
    @PostConstruct
    public void init() {
        final ServiceConfiguration configuration = configurationProducer.getServiceConfiguration();
        final BeaconInformationalResponseMeta meta = configuration.getMeta();
        if (meta != null) {
            beaconId = meta.getBeaconId();
            apiVersion = meta.getApiVersion();
        }
        
        final BeaconConfiguration config = configuration.getResponse();
        if (config != null) {
            maxPageSize = config.getMaxPageSize();
            final BeaconSecurityAttributes security_attributes = config.getSecurityAttributes();
            if (security_attributes != null) {
                defaultGranularity = security_attributes.getDefaultGranularity();
            }
        }
    }

    @Produces
    public DefaultResponseMetatada getDefaultResponseMetatada() {
        final SchemaPerEntity schema = getDefaultSchema();
        
        return new DefaultResponseMetatada(beaconId, apiVersion, 
                defaultGranularity, maxPageSize, schema);
    }
    
    private SchemaPerEntity getDefaultSchema() {
        final SchemaPerEntity schema = new SchemaPerEntity();
        
        final UriInfo uriInfo = uriInfoProducer.getUriInfo();
        final String path = uriInfo.getRequestUri().getPath();
        
        final BeaconMap map = mapResponse.getResponse();
        if (map != null) {
            final Map<String, Endpoint> endpoints = map.getEndpointSets();
            if (endpoints != null) {
                for (Endpoint endpoint : endpoints.values()) {
                    final String entryType = endpoint.getEntryType();
                    final String rootUrl = endpoint.getRootUrl();
                    if (rootUrl != null && match(rootUrl, path)) {
                        schema.setEntityType(entryType);
                        schema.setSchema(getEntryTypeSchema(entryType));
                        break;
                    }
                    final String singleEntryUrl = endpoint.getSingleEntryUrl();
                    if (singleEntryUrl != null && match(singleEntryUrl, path)) {
                        schema.setEntityType(entryType);
                        schema.setSchema(getEntryTypeSchema(entryType));
                        // set schema:
                        break;
                    }
                    
                    final Map<String, RelatedEndpoint> relEndpoints = endpoint.getEndpoints();
                    if (relEndpoints != null) {
                        for (RelatedEndpoint relEndpoint : relEndpoints.values()) {
                            final String url = relEndpoint.getUrl();
                            if (url != null && match(url, path)) {
                                final String returnedEntryType = relEndpoint.getReturnedEntryType();
                                schema.setEntityType(returnedEntryType);
                                schema.setSchema(getEntryTypeSchema(returnedEntryType));
                                break;
                            }
                        }
                    }
                }
            }
        }
        return schema;
    }
    
    private String getEntryTypeSchema(String entryType) {
        final BeaconConfiguration config = configurationProducer.getServiceConfiguration().getResponse();
        if (config != null) {
            final Map<String, EntryTypeDefinition> entryTypes = config.getEntryTypes();
            if (entryTypes != null) {
                for (EntryTypeDefinition entryTypeDefinition : entryTypes.values()) {
                    if (entryType.equals(entryTypeDefinition.getId())) {
                        final SchemaReference schema = entryTypeDefinition.getDefaultSchema();
                        if (schema != null) {
                            return schema.getReferenceToSchemaDefinition();
                        }
                    }
                }
            }
        }
        return null;
    }

    private boolean match(String endpoint, String path) {
        try {
            final String[] names1 = URI.create(
                    endpoint.replace("{", "%7B").replace("}", "%7D"))
                    .getPath().split("/");
            final String[] names2 = path.split("/");
            if (names1.length == names2.length) {
                for (int i = 0, n = names2.length; i < n; i++) {
                    if ((names1[i].startsWith("{") && names1[i].endsWith("}")) || 
                         names1[i].equals(names2[i])) {
                        continue;
                    }
                    return false;
                }
                return true;
            }
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(ResponseMetadataProducer.class.getName()).log(
                    Level.SEVERE, "invalid endpoint in the /map {0} {1}", 
                    new Object[]{endpoint, ex.getMessage()});
        }
        return false;
    }
}
