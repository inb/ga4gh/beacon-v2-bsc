/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.mongodb;

import com.mongodb.ConnectionString;
import com.mongodb.client.MongoClients;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.annotation.Priority;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Alternative;
import jakarta.enterprise.inject.Produces;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptor;
import jakarta.servlet.ServletContext;
import java.util.function.Supplier;
import org.eclipse.jnosql.databases.mongodb.communication.MongoDBDocumentConfiguration;
import org.eclipse.jnosql.databases.mongodb.communication.MongoDBDocumentManager;
import org.eclipse.jnosql.databases.mongodb.communication.MongoDBDocumentManagerFactory;

/**
 * @author Dmitry Repchevsky
 */

@Alternative
@Priority(Interceptor.Priority.APPLICATION)
@ApplicationScoped
public class DocumentManagerProducer implements Supplier<MongoDBDocumentManager> {

    private final static String MONGODB_URI_PROPERTY = "mongodb.url";
    
    @Inject 
    private ServletContext ctx;

    private MongoDBDocumentManager manager;
    
    @PostConstruct
    public void init() {
        String mongodb_url = System.getenv(MONGODB_URI_PROPERTY);
        if (mongodb_url == null || mongodb_url.isEmpty()) {
            mongodb_url = ctx.getInitParameter(MONGODB_URI_PROPERTY);
        }
        ConnectionString con = new ConnectionString(mongodb_url);
        final MongoDBDocumentManagerFactory factory = new MongoDBDocumentConfiguration()
                .get(MongoClients.create(con));
        
        manager = factory.apply(con.getDatabase());
    }
    
    @Produces
    @Override
    public MongoDBDocumentManager get() {
        return manager;
    }

    @PreDestroy
    public void destroy() {
        manager.close();
    }
}
