/**
 * *****************************************************************************
 * Copyright (C) 2024 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.mongodb;

import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import es.bsc.inb.ga4gh.beacon.config.DefaultResponseMetatada;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.common.Pagination;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.requests.AlphanumericFilter;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.requests.BeaconQueryFilter;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.requests.BeaconRequestQuery;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.requests.OntologyFilter;
import es.bsc.inb.ga4gh.beacon.ontology.OntologyFiltersProcessor;
import es.bsc.inb.ga4gh.beacon.rest.ext.BeaconAlphanumericFilter;
import es.bsc.inb.ga4gh.beacon.rest.ext.BeaconFiltersCache;
import es.bsc.inb.ga4gh.beacon.rest.ext.BeaconOntologyFilter;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.bson.BsonBoolean;
import org.bson.BsonDocument;
import org.bson.BsonInt32;
import org.bson.codecs.BsonDocumentCodec;
import org.bson.conversions.Bson;
import org.bson.json.JsonReader;

/**
 * @author Dmitry Repchevsky
 */

@Singleton
public class MongoDBQueryGenerator {

    @Inject
    private OntologyFiltersProcessor processor;
    
    @Inject
    private BeaconFiltersCache cache;

    @Inject
    private DefaultResponseMetatada meta;

    private Map<String, String> operators;
    private Map<String, String> ne_operators;
    
    @PostConstruct
    public void init() {
        operators = Map.of(
                "=", "$eq", "!", "$ne", ">", "$gt", 
                "<", "$lt", ">=", "$gte", "<=", "$lte");
        ne_operators = Map.of(
                "=", "$ne", "!", "$eq", ">", "$lte", 
                "<", "$gte", ">=", "$lt", "<=", "$gt");

    }

    /**
     * Inserts Mongodb $skip and $limit stages to the aggregation pipeline if needed.
     * 
     * @param pipeline aggregation pipeline to be updated
     * @param request_query beacon request query object
     * 
     * @return updated aggregation pipeline
     */
    public List<Bson> addPagination(List<Bson> pipeline, BeaconRequestQuery request_query) {

        if (pipeline == null) {
            pipeline = new ArrayList();
        }
            
        final Boolean test = request_query.getTestMode();
        final Integer max = Boolean.TRUE.equals(test) ? (Integer)3 : meta.maxPageSize;
        
        Integer limit = null;
        
        final Pagination pagination = request_query.getPagination();
        if (pagination != null) {
            final Integer skip = pagination.getSkip();
            if (skip != null && skip > 0) {
                pipeline.add(new BsonDocument("$skip", new BsonInt32(skip)));
            }
            
            limit = pagination.getLimit();
        }

        limit = limit == null ? max : max == null ? null : Math.min(max, limit);
        if (limit != null && limit > 0) {
            pipeline.add(new BsonDocument("$limit", new BsonInt32(limit)));
        }

        return pipeline;
    }
    
    /**
     * Prepare Mongodb BSON query like {$and: [{filter1}, {filter2}]}
     * 
     * @param scope current pipeline scope (e.g. 'biosample')
     * @param pipeline aggregation pipeline to add filters into
     * @param request_query beacon request query object
     * 
     * @return Mongodb BSON query to match filters in the request query.
     */
    public List<Bson> addMatchFilters(String scope, List<Bson> pipeline, BeaconRequestQuery request_query) {
        final Bson conditions = generateFilters(scope, request_query);
        if (conditions == null) {
            return pipeline;
        }
        if (pipeline == null) {
            pipeline = new ArrayList();
        }
        pipeline.add(Aggregates.match(conditions));
        return pipeline;
    }
    
    private Bson generateFilters(String scope, BeaconRequestQuery request_query) {
        
        final List<BeaconQueryFilter> filters = request_query.getFilters();
        if (filters != null) {
            
            final String filters_logic = request_query.getFiltersLogic();
            if (filters_logic == null) {
                final List<Bson> conditions = filters.stream()
                        .map(f -> getCondition(scope, f, false))
                        .collect(Collectors.toList());            
                return Filters.and(conditions);
            }
            
            // complex logic should be applied
            final Map<String, BeaconQueryFilter> f = filters.stream()
                .collect(Collectors.toMap(BeaconQueryFilter::getId, Function.identity()));

            final String[] logic = filters_logic.replaceAll("\\s", "")
                    .split("((?=!|&|\\||\\(|\\))|(?<=!|&|\\||\\(|\\)))");
                    
            for (String param : logic) {
                if ("!&|()".contains(param) || f.containsKey(param)) {
                    continue;
                }
                return null; //invalid query
            }

            return generateFiltersQuery(scope, logic, f, 0);
        }
        
        return null;
    }
    
    private Bson generateFiltersQuery(String scope, String[] logic,
            Map<String, BeaconQueryFilter> filters, int pos) {

        final List<Bson> conditions = new ArrayList();

        boolean negation = false;
        for (int i = pos, l = logic.length; i < l; i++) {
            final String p = logic[i];
            if (p != null) {
                if ("(".equals(p)) {
                    logic[i] = "F";
                    final Bson bson = generateFiltersQuery(scope, logic, filters, i + 1);
                    conditions.add(negation ? Filters.nor(bson) : bson);
                } else if (")".equals(p)) {
                    final String[] subquery = Arrays.copyOfRange(logic, pos, i);
                    Arrays.fill(logic, pos, i + 1, null); // clean subquery in query
                    return generateQuery(subquery, conditions);
                } else if ("!".equals(p)) {
                    negation = true;
                    logic[i] = null;
                } else if (!"&".equals(p) && !"|".equals(p)) {
                    final BeaconQueryFilter filter = filters.get(p);
                    conditions.add(getCondition(scope, filter, negation));
                }
            }
        }

        // final step (all groups are collapsed)
        return generateQuery(logic, conditions);
    }
    
    /**
     * Generate 'final' Bson query based on 'plain' query and conditions.
     * The query must not contains parenthesis and negations. Logical operations 
     * are performed according standard order ('and' then 'or'). 
     * Query may contain nulls.
     * 
     * @param logic the logical query template (i.e. 'F & F | F & F')
     * @param conditions mongodb bson queries to aggregate
     * 
     * @return final mongodb query for the filters
     */
    private Bson generateQuery(String[] logic, List<Bson> conditions) {

        final ListIterator<Bson> iter = conditions.listIterator();
        final List<Bson> group = new ArrayList(); // 'and' group

        for (int i = 0, l = logic.length; i <= l; i++) {
            final String p = i == l ? "|" : logic[i];
            if (p != null) {
                if ("|".equals(p)) {
                    final Bson f = iter.next();
                    if (!group.isEmpty()) {
                        group.add(f);
                        iter.set(Filters.and(group).toBsonDocument());
                        group.clear();
                    }
                } else if ("&".equals(p)) {
                    group.add(iter.next());
                    iter.remove();
                }
            }
        }
        
        if (conditions.size() > 1) {
            return Filters.or(conditions);
        }
        
        // all 'and' conditions has collapsed (no 'or')
        return conditions.get(0);
    }
    
    private Bson getCondition(String scope, BeaconQueryFilter filter, boolean negation) {
        if (filter instanceof OntologyFilter ontology_filter) {
            final String id = ontology_filter.getId();
            if (id != null) {
                String fscope = ontology_filter.getScope();
                if (fscope == null) {
                    fscope = scope;
                }
                final BeaconOntologyFilter filtering_term = cache.getOntologyFilterById(scope, id);
                if (filtering_term != null) {
                    final String query = filtering_term.getQuery();
                    if (query != null) {
                        final BeaconOntologyFilter f = new BeaconOntologyFilter(filtering_term);
                        f.setIncludeDescendantTerms(ontology_filter.getIncludeDescendantTerms());
                        f.setSimilarity(ontology_filter.getSimilarity());
                        return getCondition(fscope, f, negation);
                    }
                }
            }
        } else if (filter instanceof AlphanumericFilter alphanumer_filter) {
            final String id = alphanumer_filter.getId();
            if (id != null) {
                final BeaconAlphanumericFilter filtering_term = cache.getAlphanumericFilterById(scope, id);
                if (filtering_term != null) {
                    final String query = filtering_term.getQuery();
                    if (query != null) {
                        final BeaconAlphanumericFilter f = new BeaconAlphanumericFilter(filtering_term);
                        f.setOperator(alphanumer_filter.getOperator());
                        f.setValue(alphanumer_filter.getValue());
                        return getCondition(f, negation);
                    }
                }
            }
        }
        return Filters.eq(new BsonBoolean(false));
    }
    
    private Bson getCondition(String scope, BeaconOntologyFilter filter, boolean negation) {

        final StringBuilder values = new StringBuilder();
        final StringBuilder labels = new StringBuilder();
        
        values.append('"').append(filter.getId()).append('"').append(',');

        String label = filter.getLabel();
        if (label != null) {
            labels.append('"').append(label).append('"').append(',');
        }

        if (!Boolean.FALSE.equals(filter.getIncludeDescendantTerms())) {
            final String id = filter.getId();
            final String namespace = filter.getNamespace();

            final List<String> descendants = processor.getDescendants(id, namespace);
            for (String descendant : descendants) {
                final BeaconOntologyFilter subfilter = cache.getOntologyFilterByIri(scope, descendant);
                values.append('"').append(subfilter.getId()).append('"').append(',');
                label = subfilter.getLabel();
                if (label != null) {
                    labels.append('"').append(label).append('"').append(',');
                }
            }
        }
        
        final String query = filter.getQuery();
        
        final Map<String, Object> variables = new HashMap();
        final String value = "{ $in: [" + values.substring(0, values.length() - 1) + "] }";
        variables.put("value", value);
        if (query.contains("$$label")) {
            label = "{ $in: [" + (labels.isEmpty() ? "" : labels.substring(0, labels.length() - 1)) + "] }";
            variables.put("label", label);
        }
        
        final BsonDocument bson = makeQuery(query, variables);
        if (negation) {
            return Filters.nor(bson);
        }
        
        return bson;
    }
    
    private Bson getCondition(BeaconAlphanumericFilter filter, boolean negation) {
        final String id = filter.getId();
        final String value = filter.getValue();
        final String query = filter.getQuery();
        String operator = filter.getOperator();
        if (id != null && value != null && operator != null && query != null) {
            final String label = filter.getLabel();
            operator = negation ? ne_operators.getOrDefault(operator, "$ne")
                    : operators.getOrDefault(operator, "$eq");
            final Map<String, Object> variables = 
                    Map.of("id", id, "operator", operator, "value", value, "label", label);
            return makeQuery(query, variables);
        }
        
        // invalid filter
        return Filters.eq(new BsonBoolean(false));
    }
    
    private BsonDocument makeQuery(String query, Map<String, Object> variables) {
        query = setVariables(query, variables);
        final BsonDocumentCodec codec = new BsonDocumentCodec();
        try (JsonReader reader = new JsonReader(query)) {
            return codec.decode(reader, null);
        }
    }

    /**
     * Replaces '$$' prefixed variables with their values.
     * 
     * @param query the query to be processed
     * @param variables the map of variables to be set
     * 
     * @return processed query
     */
    private String setVariables(String query, Map<String, Object> variables) {
        final StringBuilder sb = new StringBuilder();
        int i = 0;
        loop:
        for(int idx; (idx = query.indexOf("$$", i)) >= 0;) {
            sb.append(query, i, idx);
            i = idx + 2;
            for (Entry<String, Object> entry : variables.entrySet()) {
                final String variable = entry.getKey();
                if (query.startsWith(variable, i)) {
                    sb.append(entry.getValue());
                    i += variable.length();
                    continue loop;
                }
            }
            sb.append("$$"); // keep variable as we didn't find in variables
        }
        
        if (sb.length() > 0) {
            sb.append(query, i, query.length());
        }
        
        return sb.toString();
    }    
}
