/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.rest;

import es.bsc.inb.ga4gh.beacon.application.BeaconMapAnalizer;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.requests.BeaconQueryFilter;
import es.bsc.inb.ga4gh.beacon.framework.rest.AsyncEndpointInterface;
import jakarta.annotation.Resource;
import jakarta.enterprise.concurrent.ManagedExecutorService;
import jakarta.inject.Inject;
import java.util.List;

/**
 * @author Dmitry Repchevsky
 */

public abstract class AbstractAsyncEndpoint implements AsyncEndpointInterface {

    @Resource
    private ManagedExecutorService executor;

    @Inject
    private BeaconMapAnalizer map;

    private String colleaction_name;
    
    @Override
    public ManagedExecutorService getExecutorService() {
        return executor;
    }

    /**
     * Get the endpoint's collection name as defined in the "beacon_map.json".
     * 
     * @return collection name
     */
    protected String getCollectionName() {
        if (colleaction_name == null) {
            colleaction_name = map.getEndpointName();
        }
        
        return colleaction_name;
    }
    
    /**
     * Converts {@code filters_query} into the 'filters_logic' and generates
     * {@code List<BeaconQueryFilter>} from it.
     * 
     * @param filters
     * @param filters_query
     * @return 
     */
    protected String getFiltersLogic(List<BeaconQueryFilter> filters, String filters_query) {
        return null;
    }
}
