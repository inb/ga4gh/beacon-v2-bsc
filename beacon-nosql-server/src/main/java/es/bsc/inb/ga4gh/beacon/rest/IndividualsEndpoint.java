/**
 * *****************************************************************************
 * Copyright (C) 2024 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.rest;

import es.bsc.inb.ga4gh.beacon.application.BeaconFilteringTermsProducer;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.common.Pagination;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.requests.BeaconQueryFilter;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.requests.BeaconRequestBody;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.requests.BeaconRequestQuery;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconFilteringTermsResponse;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconResultsetsResponse;
import es.bsc.inb.ga4gh.beacon.framework.rest.IndividualsEndpointInterface;
import es.bsc.inb.ga4gh.beacon.rest.ext.BeaconFiltersConverter;
import es.bsc.inb.ga4gh.beacon.service.BiosamplesService;
import es.bsc.inb.ga4gh.beacon.service.GenomicVariationsService;
import es.bsc.inb.ga4gh.beacon.service.IndividualsService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.ext.ParamConverter;
import java.util.List;

/**
 * @author Dmitry Repchevsky
 */

@ApplicationScoped
public class IndividualsEndpoint extends AbstractAsyncEndpoint
        implements IndividualsEndpointInterface {
    
    @Inject 
    private BeaconFilteringTermsProducer filtering_terms;
            
    @Inject 
    private IndividualsService individuals_service;
    
    @Inject 
    private BiosamplesService biosamples_service;
    
    @Inject 
    private GenomicVariationsService variants_service;
    
    @Override
    public BeaconResultsetsResponse getIndividuals(
            String requested_schema, Integer skip, Integer limit, 
            String include_responses, List<BeaconQueryFilter> filters,
            String filters_query) {

        final BeaconRequestQuery query = new BeaconRequestQuery();
        
        if (skip != null || limit != null) {
            query.setPagination(new Pagination(skip, limit));
        }

        query.setIncludeResultsetResponses(include_responses);
        
        if (filters_query != null && !filters_query.isBlank()) {
            filters_query = filters_query.replace(" and ", " & ");
            filters_query = filters_query.replace(" or ", " | ");
            filters_query = filters_query.replace(" not ", " ! ");
            
            filters = BeaconFiltersConverter.fromQuery(filters_query);
            for (BeaconQueryFilter f : filters) {
                filters_query = filters_query.replaceAll(f.toString(), f.getId());
            }
            query.setFiltersLogic(filters_query);
        }

        if (filters != null && !filters.isEmpty()) {
            query.setFilters(filters);
        }

        final BeaconRequestBody request = new BeaconRequestBody();
        request.setQuery(query);
        request.setSchema(requested_schema);
        
        return individuals_service.getBeaconResponse(request);
    }

    @Override
    public BeaconResultsetsResponse postIndividualsRequest(BeaconRequestBody request) {
        return individuals_service.getBeaconResponse(request);
    }

    @Override
    public BeaconResultsetsResponse getOneIndividual(String id) {
        return individuals_service.getBeaconResponse(id, null);
    }

    @Override
    public BeaconResultsetsResponse postOneIndividualRequest(
            String id, BeaconRequestBody request) {

        return individuals_service.getBeaconResponse(id, request);
    }

    @Override
    public BeaconResultsetsResponse getOneIndividualGenomicVariants(String id, 
            String requested_schema, Integer skip, Integer limit) {
        
        BeaconRequestQuery query = new BeaconRequestQuery();
        query.setPagination(new Pagination(skip, limit));
        
        BeaconRequestBody request = new BeaconRequestBody();
        request.setQuery(query);
        request.setSchema(requested_schema);
        
        return variants_service.getIndividualGenomicVariants(id, request);
    }

    @Override
    public BeaconResultsetsResponse postOneIndividualGenomicVariantsRequest(String id, 
            BeaconRequestBody request) {

        return variants_service.getIndividualGenomicVariants(id, request);
    }

    @Override
    public BeaconResultsetsResponse getOneIndividualBiosamples(String id, 
            String requested_schema, Integer skip, Integer limit) {

        BeaconRequestQuery query = new BeaconRequestQuery();
        if (skip != null || limit != null) {
            query.setPagination(new Pagination(skip, limit));
        }
        
        BeaconRequestBody request = new BeaconRequestBody();
        request.setQuery(query);
        request.setSchema(requested_schema);
        
        return biosamples_service.getIndividualBiosamples(id, request);
    }

    @Override
    public BeaconResultsetsResponse postOneIndividualBiosamplesRequest(String id,
            BeaconRequestBody request) {

        return biosamples_service.getIndividualBiosamples(id, request);
    }

    @Override
    public BeaconFilteringTermsResponse getIndividualsFilteringTerms(Integer skip, 
            Integer limit) {
        return filtering_terms.getFilteringTerms();
    }

    @Override
    public BeaconFilteringTermsResponse postIndividualsFilteringTermsRequest(BeaconRequestBody request) {
        return filtering_terms.getFilteringTerms();
    }
    
}
