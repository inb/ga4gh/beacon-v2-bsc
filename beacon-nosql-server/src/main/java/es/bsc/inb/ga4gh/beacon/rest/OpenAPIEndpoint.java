package es.bsc.inb.ga4gh.beacon.rest;

import es.bsc.inb.ga4gh.beacon.config.BeaconOpenAPIProducer;
import jakarta.inject.Inject;
import jakarta.json.JsonObject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;

/**
 * @author Dmitry Repchevsky
 */

@Path(value = "/")
public class OpenAPIEndpoint {
    
    @Inject
    private BeaconOpenAPIProducer openapi_producer;
    
    @GET
    @Path("/{endpoint}-openapi.json")
    @Produces(value = {"application/json"})
    public JsonObject getResourceMetadata(
            @Context UriInfo info,
            @PathParam("endpoint") String endpoint) {
        return openapi_producer.getOpenAPI(info.getAbsolutePath().toString());
    }
}
