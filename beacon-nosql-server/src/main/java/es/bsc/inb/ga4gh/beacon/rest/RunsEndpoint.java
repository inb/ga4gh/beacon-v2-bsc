/**
 * *****************************************************************************
 * Copyright (C) 2024 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.rest;

import es.bsc.inb.ga4gh.beacon.application.BeaconFilteringTermsProducer;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.common.Pagination;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.requests.BeaconQueryFilter;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.requests.BeaconRequestBody;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.requests.BeaconRequestQuery;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconFilteringTermsResponse;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconResultsetsResponse;
import es.bsc.inb.ga4gh.beacon.framework.rest.RunsEndpointInterface;
import es.bsc.inb.ga4gh.beacon.rest.ext.BeaconFiltersConverter;
import es.bsc.inb.ga4gh.beacon.service.AnalysesService;
import es.bsc.inb.ga4gh.beacon.service.GenomicVariationsService;
import es.bsc.inb.ga4gh.beacon.service.RunsService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.util.List;

/**
 * @author Dmitry Repchevsky
 */

@ApplicationScoped
public class RunsEndpoint extends AbstractAsyncEndpoint 
        implements RunsEndpointInterface {

    @Inject
    private BeaconFilteringTermsProducer filtering_terms;

    @Inject 
    private RunsService runs_service;
    
    @Inject 
    private AnalysesService analyses_service;
    
    @Inject 
    private GenomicVariationsService variants_service;

    @Override
    public BeaconResultsetsResponse getRuns(String requested_schema, Integer skip, 
            Integer limit, String include_responses, List<BeaconQueryFilter> filters,
            String filters_query) {

        final BeaconRequestQuery query = new BeaconRequestQuery();
        
        if (skip != null || limit != null) {
            query.setPagination(new Pagination(skip, limit));
        }
        
        query.setIncludeResultsetResponses(include_responses);

        if (filters_query != null && !filters_query.isBlank()) {
            filters_query = filters_query.replace(" and ", " & ");
            filters_query = filters_query.replace(" or ", " | ");
            filters_query = filters_query.replace(" not ", " ! ");
            
            filters = BeaconFiltersConverter.fromQuery(filters_query);
            for (BeaconQueryFilter f : filters) {
                filters_query = filters_query.replaceAll(f.toString(), f.getId());
            }
            query.setFiltersLogic(filters_query);
        }
        
        if (filters != null && !filters.isEmpty()) {
            query.setFilters(filters);
        }

        final BeaconRequestBody request = new BeaconRequestBody();
        request.setQuery(query);
        request.setSchema(requested_schema);
        
        return runs_service.getBeaconResponse(request);
    }

    @Override
    public BeaconResultsetsResponse postRunsRequest(BeaconRequestBody request) {
        return runs_service.getBeaconResponse(request);
    }

    @Override
    public BeaconResultsetsResponse getOneRun(String id) {
        return runs_service.getBeaconResponse(id, null);
    }

    @Override
    public BeaconResultsetsResponse postOneRun(String id, 
            BeaconRequestBody request) {

        return runs_service.getBeaconResponse(id, request);
    }

    @Override
    public BeaconResultsetsResponse getOneRunGenomicVariants(String id, 
            String requested_schema, Integer skip, Integer limit) {

        BeaconRequestQuery query = new BeaconRequestQuery();
        if (skip != null || limit != null) {
            query.setPagination(new Pagination(skip, limit));
        }
        
        BeaconRequestBody request = new BeaconRequestBody();
        request.setQuery(query);
        request.setSchema(requested_schema);
        
        return postOneRunGenomicVariantsRequest(id, request);
    }

    @Override
    public BeaconResultsetsResponse postOneRunGenomicVariantsRequest(String id, 
            BeaconRequestBody request) {

        return variants_service.getRunGenomicVariants(id, request);
    }

    @Override
    public BeaconResultsetsResponse getOneRunAnalyses(String id, 
            String requested_schema, Integer skip, Integer limit) {

        BeaconRequestQuery query = new BeaconRequestQuery();
        if (skip != null || limit != null) {
            query.setPagination(new Pagination(skip, limit));
        }
        
        BeaconRequestBody request = new BeaconRequestBody();
        request.setQuery(query);
        request.setSchema(requested_schema);
        
        return analyses_service.getRunsAnalyses(id, request);    
    }

    @Override
    public BeaconResultsetsResponse postOneRunAnalysesRequest(String id, 
            BeaconRequestBody request) {

        return analyses_service.getRunsAnalyses(id, request);  
    }

    @Override
    public BeaconFilteringTermsResponse getRunsFilteringTerms(Integer skip, Integer limit) {
        return filtering_terms.getFilteringTerms();
    }

    @Override
    public BeaconFilteringTermsResponse postRunsFilteringTermsRequest(BeaconRequestBody request) {
        return filtering_terms.getFilteringTerms();
    }
}
