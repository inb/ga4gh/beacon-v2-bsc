/**
 * *****************************************************************************
 * Copyright (C) 2024 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.rest.ext;

import es.bsc.inb.ga4gh.beacon.application.BeaconFilteringTermsProducer;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconFilteringTermsResponse;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconFilteringTermsResults;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.FilteringTerm;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.Resource;
import es.bsc.inb.ga4gh.beacon.jsonb.BeaconFilteringTerm;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Dmitry Repchevsky
 */

@Singleton
public class BeaconFiltersCache {

    @Inject
    private BeaconFilteringTermsProducer filtering_terms;
    
    /**
     * The map of known ontologies´ namespaces by their scope and prefix. 
     * (i.g. Map&lt;'biosample:NCIT', 'http://ncicb.nci.nih.gov/xml/owl/EVS/Thesaurus.owl#'&gt;)
     * 
     */
    private Map<String, String> namespaces;
    
    /**
     * The map of ontological filters by scoped term's IRI
     * (i.g. Map&lt;'biosample:http://ncicb.nci.nih.gov/xml/owl/EVS/Thesaurus.owl#C16576', obj)
     */
    private Map<String, BeaconOntologyFilter> ontology_filters;

    /**
     * The map of alphanumeric filters by scoped ids.
     * (i.g. Map&lt;'biosample:age', obj)
     */
    private Map<String, BeaconAlphanumericFilter> alphanumeric_filters;
    
    @PostConstruct
    public void init() {
        namespaces = new ConcurrentHashMap();
        ontology_filters = new ConcurrentHashMap();
        alphanumeric_filters = new ConcurrentHashMap();
        
        final Map<String, BeaconFilteringTermsResponse> responses = 
                filtering_terms.getFilteringTermsResponses();
        
        if (responses != null) {
            for (Map.Entry<String, BeaconFilteringTermsResponse> entry : responses.entrySet()) {
                final BeaconFilteringTermsResponse response = entry.getValue();
                if (response != null) {
                    final BeaconFilteringTermsResults results = response.getResponse();
                    if (results != null) { 
                        final String scope = entry.getKey();
                        final List<Resource> resources = results.getResources();
                        if (resources != null) {
                            for (Resource resource : resources) {
                                final String prefix = resource.getNameSpacePrefix();
                                final String namespace = resource.getIriPrefix();
                                if (prefix != null && namespace != null) {
                                    namespaces.put(scope + ":" + prefix, namespace);
                                }
                            }
                        }
                        final List<FilteringTerm> terms = results.getFilteringTerms();
                        if (terms != null) {
                            for (FilteringTerm term : terms) {
                                final String id = term.getId();
                                final String type = term.getType();
                                if (type == null) {
                                    Logger.getLogger(BeaconFilteringTermsProducer.class.getName()).log(
                                            Level.WARNING, "missed filter type for {0}", id);
                                } else {
                                    final String label = term.getLabel();
                                    if (term instanceof BeaconFilteringTerm xterm) {
                                        final String query = xterm.getQuery();
                                        if (query != null) {
                                            switch(type) {
                                                case "ontology": {
                                                        final BeaconOntologyFilter filter = 
                                                                new BeaconOntologyFilter(id, null, label, xterm.getNamespace(), scope, null, query);
                                                        final int idx = id.indexOf(':');
                                                        if (idx > 0) { 
                                                            ontology_filters.put(scope + ":" + xterm.getNamespace() + id.substring(idx + 1), filter);
                                                        }
                                                    }
                                                    break;
                                                case "alphanumeric": {
                                                        final BeaconAlphanumericFilter filter = 
                                                                new BeaconAlphanumericFilter(id, label, scope, null, null, query);
                                                        alphanumeric_filters.put(scope + ":" + id, filter);
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    public BeaconOntologyFilter getOntologyFilterByIri(String scope, String iri) {
        return ontology_filters.get(scope + ":" + iri);
    }

    public BeaconAlphanumericFilter getAlphanumericFilterById(String scope, String id) {
        BeaconAlphanumericFilter filter = alphanumeric_filters.get(scope + ":" + id);
        if (filter == null) {
            for (BeaconAlphanumericFilter f : alphanumeric_filters.values()) {
                if (id.equals(f.getLabel()) && scope.equals(f.getScope())) {
                    return f;
                }
            }
        }
        return filter;
    }
    
    public BeaconOntologyFilter getOntologyFilterById(String scope, String id) {
        final int idx = id.indexOf(':');
        if (idx > 0) {
            if (scope != null) {
                final String prefix = id.substring(0, idx);
                String namespace = namespaces.get(scope + ":" + prefix);
                if (namespace != null) {
                    return ontology_filters.get(scope + ":" + namespace + id.substring(idx + 1));
                }
            }
        }
        return getFilterByLabel(scope, id);
    }

    private BeaconOntologyFilter getFilterByLabel(String scope, String value) {
        final BeaconFilteringTermsResponse response = filtering_terms.getFilteringTerms();
        if (response != null) {
            final BeaconFilteringTermsResults results = response.getResponse();
            if (results != null) {
                final List<FilteringTerm> terms = results.getFilteringTerms();
                if (terms != null) {
                    for (FilteringTerm term : terms) {
                        final String id = term.getId();
                        final String label = term.getLabel();
                        if ((value.equals(id) || value.equalsIgnoreCase(label)) 
                            && term instanceof BeaconFilteringTerm) {
                            final BeaconFilteringTerm xterm = ((BeaconFilteringTerm)term);
                            final String query = xterm.getQuery();
                            if (query != null) {
                                final BeaconOntologyFilter filter = 
                                        new BeaconOntologyFilter(id, null, label, xterm.getNamespace(), null, null, query);
                                final int idx = id.indexOf(':');
                                if (idx > 0) { 
                                    ontology_filters.put(scope + ":" + xterm.getNamespace() + id.substring(idx + 1), filter);
                                }
                                return filter;
                            }
                        }
                    }
                }
            }
        }
        
        return null;
    }
}
