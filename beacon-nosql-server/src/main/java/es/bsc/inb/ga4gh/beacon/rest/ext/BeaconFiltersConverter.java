/**
 * *****************************************************************************
 * Copyright (C) 2024 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.rest.ext;

import es.bsc.inb.ga4gh.beacon.framework.model.v200.requests.AlphanumericFilter;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.requests.BeaconQueryFilter;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.requests.OntologyFilter;
import jakarta.ws.rs.ext.ParamConverter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Dmitry Repchevsky
 */

public class BeaconFiltersConverter
        implements ParamConverter<List<BeaconQueryFilter>> {

    public final static String FILTERS_QUERY_PARAM_NAME = "filters_query";
    
    private final URIInfoProducer uriInfoProducer;
    private final String param_name;
    
    public BeaconFiltersConverter(URIInfoProducer uriInfoProducer, 
            String param_name) {
        this.uriInfoProducer = uriInfoProducer;
        this.param_name = param_name;
    }

    @Override
    public List<BeaconQueryFilter> fromString(String value) {
        final List<BeaconQueryFilter> list = new ArrayList();
        
        final List<String> params = uriInfoProducer.getUriInfo()
                .getQueryParameters().getOrDefault(param_name, List.of(value));
        for (String param : params) {
            addFilters(param, list);
        }
        
        return list;
    }

    public static List<BeaconQueryFilter> fromQuery(String filters_query) {
        // extract a list of filters from query
        final String filters = filters_query.replaceAll("\\(|\\)|!|\\s", "")
                .replaceAll("&|\\|", ",");
        return addFilters(filters, new ArrayList());
    }
    
    private static List<BeaconQueryFilter> addFilters(String param, List<BeaconQueryFilter> list) {
        for (String val : param.split(",")) {
            final String eq[] = val.split("=|<|>|!|>=|<=");
            switch (eq.length) {
                case 1:
                    list.add(new OntologyFilter(val));
                    break;
                case 2:
                    final String op = val.substring(eq[0].length(), val.length() - eq[1].length());
                    list.add(new AlphanumericFilter(eq[0], op, eq[1]));
                    break;
            }
        }
        return list;
    }
    
    @Override
    public String toString(List<BeaconQueryFilter> filters) {
        return filters.stream().map(f -> ((OntologyFilter)f).getId())
                .collect(Collectors.joining(","));
    }
}
