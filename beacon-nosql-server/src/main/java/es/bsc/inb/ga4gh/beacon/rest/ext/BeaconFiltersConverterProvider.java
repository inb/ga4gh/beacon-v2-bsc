/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.rest.ext;

import es.bsc.inb.ga4gh.beacon.framework.model.v200.requests.BeaconQueryFilter;
import jakarta.inject.Inject;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.ext.ParamConverter;
import jakarta.ws.rs.ext.ParamConverterProvider;
import jakarta.ws.rs.ext.Provider;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

/**
 * Provides filtering terms converter for query parameters.
 * 
 * @author Dmitry Repchevsky
 */

@Provider
public class BeaconFiltersConverterProvider implements ParamConverterProvider {

    @Inject
    private URIInfoProducer uriInfoProducer;
    
    @Override
    public <T> ParamConverter<T> getConverter(
            Class<T> clazz, Type genericType, Annotation[] annotations) {

        if (clazz.isAssignableFrom(List.class) && 
            genericType instanceof ParameterizedType &&
            ((ParameterizedType)genericType).getActualTypeArguments()[0]
                    .equals(BeaconQueryFilter.class)) {
            QueryParam param = Arrays.stream(annotations)
                .filter(QueryParam.class::isInstance)
                .map(QueryParam.class::cast)
                .findFirst().orElse(null);
            if (param != null) {
                return (ParamConverter<T>)new BeaconFiltersConverter(uriInfoProducer, param.value());
            }
        }
        return null;
    }
}
