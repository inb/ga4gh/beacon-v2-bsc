/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.service;

import es.bsc.inb.ga4gh.beacon.application.BeaconMapAnalizer;
import es.bsc.inb.ga4gh.beacon.config.DefaultResponseMetatada;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.common.Granularity;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.common.Pagination;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.requests.BeaconQueryFilter;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.requests.BeaconRequestBody;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.requests.BeaconRequestMeta;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.requests.BeaconRequestQuery;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconReceivedRequestSummary;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconResponse;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconResponseMeta;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconResponseSummary;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconResultset;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconResultsets;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconResultsetsResponse;
import es.bsc.inb.ga4gh.beacon.mongodb.MongoDBQueryGenerator;
import jakarta.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.bson.BsonDocument;
import org.bson.BsonString;
import org.bson.BsonValue;
import org.bson.conversions.Bson;
import org.eclipse.jnosql.databases.mongodb.mapping.MongoDBTemplate;

/**
 * @author Dmitry Repchevsky
 */

public abstract class AbstractBeaconService {

    @Inject
    protected DefaultResponseMetatada meta;

    @Inject
    protected MongoDBTemplate template;

    @Inject
    protected MongoDBQueryGenerator queryBuilder;

    @Inject
    protected BeaconMapAnalizer map;
    
    private String entry_type = null;
    private String colleaction_name = null;

    /**
     * Return the endpoint's name (which is used as a collection name) 
     * for the service.
     * 
     * @return collection name of this service
     */
    protected String getCollectionName() {
        if (colleaction_name == null) {
            colleaction_name = map.getEndpointName(map.getCurrentEntryType());
        }
        
        return colleaction_name;
    }
    
    /**
     * Method returns the service 'entryType' as defined in the "beacon_map.json".
     * <pre>
     * Each service serves exactly one entryType and may be used by many endpoints.
     * For instance "/biosamples" and "/individuals/{id}/biosamples" 
     * return "biosample" 'entryType'.
     * </pre>
     * 
     * @return 'entryType' name of this service
     */
    private String getEntryType() {
        if (entry_type == null) {
            entry_type = map.getCurrentEntryType();
        }
        
        return entry_type;
    }
    
    protected abstract <T extends BeaconResponse> T makeBooleanResponse(
            boolean exists, BeaconRequestBody request);
    
    protected abstract <T extends BeaconResponse> T makeCountResponse(
            long count, BeaconRequestBody request);

    protected abstract <T extends BeaconResponse> T makeResultsetsResponse(
            List beacons, long count, BeaconRequestBody request);
    
    protected List<Bson> prepareAggregation(List<Bson> pipeline, BeaconRequestQuery request_query) {
        return queryBuilder.addMatchFilters(getEntryType(), pipeline, request_query);
    }

    private long countEntities(List<Bson> pipeline) {
        if (pipeline == null || pipeline.isEmpty()) {
            return template.count(getCollectionName());
        }
        
        final Bson[] aggregation = pipeline.toArray(new Bson[pipeline.size() + 1]);
        aggregation[pipeline.size()] = new BsonDocument("$count", new BsonString("count"));
        
        final Optional<Map<String,BsonValue>> res = template.aggregate(getCollectionName(), aggregation).findFirst();
        
        return res.isEmpty() ? 0 : res.get().get("count").asNumber().longValue();
    }

    public <T extends Object> T findEntity(String id) {
        try {
            Stream<T> stream = template.select(getCollectionName(), 
                    new BsonDocument("id", new BsonString(id)));
            Optional<T> optional = stream.findFirst();
            return optional.orElse(null);
        } catch (Throwable th) {
            Logger.getLogger(AbstractBeaconService.class.getName())
                    .log(Level.SEVERE, th.getMessage());
        }
        return null;
    }
    
    private List findEntities(List pipeline) {
        return template.aggregate(getCollectionName(), pipeline).toList();
    }

    protected BeaconResultsetsResponse getBooleanResponse(
            boolean exists, BeaconRequestBody request) {

        BeaconResultsetsResponse response = new BeaconResultsetsResponse();
        response.setResponseSummary(new BeaconResponseSummary(exists));
        response.setMeta(getMeta(request));

        return response;
    }

    protected BeaconResultsetsResponse getCountResponse(
            long count, BeaconRequestBody request) {
        BeaconResultsetsResponse response = new BeaconResultsetsResponse();

        response.setResponseSummary(new BeaconResponseSummary((int)count));
        
        if (count != 0) {
            BeaconResultset resultset = new BeaconResultset();
            resultset.setExists(true);
            resultset.setResultsCount((int)count);

            BeaconResultsets resultsets = new BeaconResultsets();
            resultsets.setResultSets(Arrays.asList(resultset));

            response.setResponse(resultsets);
        }
        response.setMeta(getMeta(request));
        
        return response;
    }
    
    protected BeaconResultsetsResponse getResultsetsResponse(
            List beacons, long count, BeaconRequestBody request) {
        BeaconResultsetsResponse response = new BeaconResultsetsResponse();

        response.setResponseSummary(new BeaconResponseSummary((int)count));

        BeaconResultset resultset = new BeaconResultset();
        resultset.setId(meta.beaconId);
        resultset.setSetType(meta.schema.getEntityType());
        resultset.setExists(count > 0);
        resultset.setResultsCount((int)count);
        resultset.setResults(beacons);

        BeaconResultsets resultsets = new BeaconResultsets();
        resultsets.setResultSets(Arrays.asList(resultset));

        response.setResponse(resultsets);

        response.setMeta(getMeta(request));

        return response;
    }

    public BeaconResultsetsResponse getBeaconResponse(String id, BeaconRequestBody request) {
        final Object entity = findEntity(id);
        if (entity == null) {
            return getResultsetsResponse(Collections.EMPTY_LIST, 0, request);
        }
            
        return getResultsetsResponse(List.of(entity), 1, request);
    }

    public <T extends BeaconResponse> T getBeaconResponse(BeaconRequestBody request) {
        return (T) getBeaconResponse(request, new ArrayList());
    }
    
    public <T extends BeaconResponse> T getBeaconResponse(BeaconRequestBody request, List<Bson> pipeline) {
        try {
            final BeaconRequestQuery request_query = request.getQuery();
            
            List<Bson> aggregation = prepareAggregation(pipeline, request_query);
            aggregation = queryBuilder.addPagination(aggregation, request_query);
          
            final Granularity granularity = getGranularity(request);
            if (granularity == null || granularity == Granularity.RECORD) {
                final long count = countEntities(aggregation);
                final List beacons = findEntities(aggregation);
                return makeResultsetsResponse(beacons, count, request);
            }
            
            if (granularity == Granularity.COUNT) {
                final long count = countEntities(aggregation);
                return makeCountResponse(count, request);
            }
            
            if (granularity == Granularity.BOOLEAN) {
                final long count = countEntities(aggregation);
                return makeBooleanResponse(count > 0, request);
            }
        } catch (Throwable th) {
            Logger.getLogger(AbstractBeaconService.class.getName())
                    .log(Level.SEVERE, th.getMessage());
        }
        
        return null;
    }
    
    /**
     * Auxiliary method to get granularity the Beacon request.
     * If no granularity found in request, defaultGranularity from configuration 
     * is returned.
     * 
     * @param request Beacon request object
     * @return granularity enum
     */    
    protected Granularity getGranularity(BeaconRequestBody request) {
        if (request != null) {
            final BeaconRequestQuery request_query = request.getQuery();
            if (request_query != null) {
                final String granularity = request_query.getRequestedGranularity();
                if (granularity != null) {
                    try {
                        return Granularity.getGranularity(granularity);
                    } catch (IllegalArgumentException ex) {}
                }
            }
        }
        if (meta.defaultGranularity != null) {
            try {
                return Granularity.getGranularity(meta.defaultGranularity);
            } catch (IllegalArgumentException ex) {}
        }
        
        return Granularity.COUNT;
    }

    protected BeaconResponseMeta getMeta(BeaconRequestBody request) {
        final BeaconResponseMeta response_meta = new BeaconResponseMeta();

        final BeaconReceivedRequestSummary request_summary = 
                new BeaconReceivedRequestSummary();

        if (request != null) {
            final BeaconRequestMeta request_meta = request.getMeta();
            if (request_meta != null) {
                request_summary.setApiVersion(request_meta.getApiVersion());
                request_summary.setRequestedSchemas(request_meta.getRequestedSchemas());
            }
            
            final BeaconRequestQuery request_query = request.getQuery();  
            if (request_query != null) {
                request_summary.setPagination(request_query.getPagination());
                request_summary.setBeaconRequestParameters(request_query.getRequestParameters());
                
                final List<BeaconQueryFilter> filters = request_query.getFilters();
                if (filters != null) {
                    request_summary.setFilters(filters.stream()
                            .filter(Objects::nonNull)
                            .map(BeaconQueryFilter::toString)
                            .collect(Collectors.toList()));
                }

                request_summary.setRequestedGranularity(request_query.getRequestedGranularity());
                request_summary.setTestMode(request_query.getTestMode());
            }
        }
        
        if (request_summary.getApiVersion() == null) {
            request_summary.setApiVersion(meta.apiVersion);
        }

        if (request_summary.getRequestedGranularity() == null) {
            request_summary.setRequestedGranularity(meta.defaultGranularity);
        }

        if (request_summary.getRequestedSchemas() == null) {
            request_summary.setRequestedSchemas(Collections.EMPTY_LIST);
        }
        
        if (request_summary.getPagination() == null) {
            request_summary.setPagination(new Pagination());
        }

        response_meta.setReceivedRequestSummary(request_summary);
      
        response_meta.setBeaconId(meta.beaconId);
        response_meta.setApiVersion(meta.apiVersion);

        response_meta.setReturnedGranularity(request_summary.getRequestedGranularity());

        if (meta.schema != null) {
            response_meta.setReturnedSchemas(Arrays.asList(meta.schema));
        }

        return response_meta;
    }

}
