/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.service;

import es.bsc.inb.ga4gh.beacon.framework.model.v200.requests.BeaconRequestBody;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconResultsetsResponse;
import java.util.List;
import jakarta.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import org.bson.BsonArray;
import org.bson.BsonBoolean;
import org.bson.BsonDocument;
import org.bson.BsonString;
import org.bson.conversions.Bson;

/**
 * @author Dmitry Repchevsky
 */

@ApplicationScoped
public class BiosamplesService extends BeaconService {

    public BeaconResultsetsResponse getIndividualBiosamples(String id, BeaconRequestBody request) {
        final List<Bson> pipeline = Arrays.asList(new BsonDocument("$match", 
                new BsonDocument("individualId", new BsonString(id))));
        return getBeaconResponse(request, new ArrayList<>(pipeline));
    }

    public BeaconResultsetsResponse getGenomicVariationBiosamples(
            String variationsCollection, String id, BeaconRequestBody request) {
        
        // emulate inner join via "$lookup". 
        // N.B. jnosql returns same entries as called collection.
        final List<Bson> pipeline = new ArrayList(Arrays.asList(
                new BsonDocument("$lookup", new BsonDocument()
                        .append("from", new BsonString(variationsCollection))
                        .append("localField", new BsonString("id"))
                        .append("foreignField", new BsonString("caseLevelData.biosampleId"))
                        .append("pipeline", new BsonArray(List.of(
                                new BsonDocument("$match", new BsonDocument("variantInternalId", new BsonString(id))))))
                        .append("as", new BsonString(variationsCollection))),
                new BsonDocument("$match", new BsonDocument(variationsCollection, new BsonDocument("$ne", new BsonArray()))),
                new BsonDocument("$project", new BsonDocument(variationsCollection, new BsonBoolean(false)))
        ));
        
        return getBeaconResponse(request, pipeline);
    }
    
    public BeaconResultsetsResponse getDatasetBiosamples(
            String datasetsCollection, String id, BeaconRequestBody request) {
        // not implemented
        return makeResultsetsResponse(Collections.EMPTY_LIST, 0, request);
    }
}
