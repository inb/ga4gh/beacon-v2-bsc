package es.bsc.inb.ga4gh.beacon.service;

import es.bsc.inb.ga4gh.beacon.framework.model.v200.requests.BeaconRequestBody;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.requests.BeaconRequestParameters;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconCollections;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconCollectionsResponse;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconResponseSummary;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconResultset;
import java.util.List;

/**
 * @author Dmitry Repchevsky
 * 
 * @param <L> BeaconRequestParameters implementation class
 */

public abstract class CollectionsBeaconService <L extends BeaconRequestParameters>
        extends AbstractBeaconService {
    
    @Override
    protected BeaconCollectionsResponse makeBooleanResponse(
            boolean exists, BeaconRequestBody request) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    protected BeaconCollectionsResponse makeCountResponse(
            long count, BeaconRequestBody request) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    protected BeaconCollectionsResponse makeResultsetsResponse(
            List beacons, long count, BeaconRequestBody request) {
        
        final BeaconCollectionsResponse response = new BeaconCollectionsResponse();
        response.setResponseSummary(new BeaconResponseSummary((int)count));
        
        if (count > 0) {
            final BeaconResultset resultset = new BeaconResultset();
            resultset.setExists(true);
            resultset.setResultsCount((int)count);
            resultset.setResults(beacons);

            final BeaconCollections collections = new BeaconCollections();
            collections.setCollections(beacons);

            response.setResponse(collections);
        }
        response.setMeta(getMeta(request));
        return response;
    }

}
