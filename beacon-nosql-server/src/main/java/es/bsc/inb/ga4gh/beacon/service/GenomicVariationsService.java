/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.service;

import com.mongodb.client.model.Filters;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.requests.BeaconRequestBody;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconResultsetsResponse;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.GenomicVariationsRequestParameters;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.requests.BeaconRequestQuery;
import es.bsc.inb.ga4gh.beacon.nosql.VariantEntity;
import jakarta.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import org.bson.BsonDocument;
import org.bson.BsonString;
import org.bson.conversions.Bson;

/**
 * @author Dmitry Repchevsky
 */

@ApplicationScoped
public class GenomicVariationsService extends BeaconService {

    public BeaconResultsetsResponse getAnalysisGenomicVariants(String analysisId, 
            BeaconRequestBody request) {
        final List<Bson> pipeline = Arrays.asList(new BsonDocument("$match", 
                new BsonDocument("caseLevelData.analysisId", new BsonString(analysisId))));
        return getBeaconResponse(request, new ArrayList<>(pipeline));
    }

    public BeaconResultsetsResponse getIndividualGenomicVariants(String individualId, 
            BeaconRequestBody request) {
        final List<Bson> pipeline = Arrays.asList(new BsonDocument("$match", 
                new BsonDocument("caseLevelData.individualId", new BsonString(individualId))));
        return getBeaconResponse(request, new ArrayList<>(pipeline));
    }

    public BeaconResultsetsResponse getBiosampleGenomicVariants(String biosampleId, 
            BeaconRequestBody request) {
        final List<Bson> pipeline = Arrays.asList(new BsonDocument("$match", 
                new BsonDocument("caseLevelData.biosampleId", new BsonString(biosampleId))));
        return getBeaconResponse(request, new ArrayList<>(pipeline));
    }

    public BeaconResultsetsResponse getRunGenomicVariants(String runId, 
            BeaconRequestBody request) {
        final List<Bson> pipeline = Arrays.asList(new BsonDocument("$match", 
                new BsonDocument("caseLevelData.runId", new BsonString(runId))));
        return getBeaconResponse(request, new ArrayList<>(pipeline));

    }

    public BeaconResultsetsResponse getDatasetGenomicVariants(
            String datasetsCollection, String datasetId, BeaconRequestBody request) {
        // not implemented
        return makeResultsetsResponse(Collections.EMPTY_LIST, 0, request);
    }    
    /**
     * Method is overridden because of some baffling reason the "id" property 
     * in the Genomic variation object is "variantInternalId".
     * 
     * @param id the variantInternalId
     * 
     * @return found variant or null
     */
    @Override
    public VariantEntity findEntity(String id) {
        try {
            final Stream<VariantEntity> stream = template.select(getCollectionName(), 
                    new BsonDocument("variantInternalId", new BsonString(id)));
            final Optional<VariantEntity> optional = stream.findFirst();
            return optional.orElse(null);
        } catch (Throwable th) {
            Logger.getLogger(AbstractBeaconService.class.getName())
                    .log(Level.SEVERE, th.getMessage());
        }
        return null;
    }

    /**
     * Overridden method that adds genomic variation parameters to the MongoDB query.
     * 
     * @param pipeline aggregation pipeline
     * @param request_query beacon request query object
     * 
     * @return MongoDB condition query
     */
    @Override
    protected List<Bson> prepareAggregation(List<Bson> pipeline, BeaconRequestQuery request_query) {
        
        List<Bson> aggregation = super.prepareAggregation(pipeline, request_query);
        
        final GenomicVariationsRequestParameters params = 
                (GenomicVariationsRequestParameters)request_query.getRequestParameters();

        if (params == null) {
            return aggregation;
        }
        
        final List conditions = new ArrayList();

        final String variantType = params.getVariantType();
        if (variantType != null) {
            conditions.add(new BsonDocument("variantType", new BsonString(variantType)));
        }

        final String referenceBases = params.getReferenceBases();
        if (referenceBases != null) {
            conditions.add(new BsonDocument("referenceBases", new BsonString(referenceBases)));
        }

        final String alternateBases = params.getAlternateBases();
        if (alternateBases != null) {
            conditions.add(new BsonDocument("alternateBases", new BsonString(alternateBases)));
        }

        final String assemblyId = params.getAssemblyId();
        if (assemblyId != null) {
            conditions.add(new BsonDocument("position.assemblyId", new BsonString(assemblyId)));
        }
        
        final String refseqId = params.getAssemblyId();
        if (refseqId != null) {
            conditions.add(new BsonDocument("position.refseqId", new BsonString(refseqId)));
        }

        final long[] start = params.getStart();
        if (start != null && start.length > 0) {
            conditions.add(Filters.gte("position.start", start[0]));
            if (start.length > 1) {
                conditions.add(Filters.lte("position.start", start[1]));
            }
        }

        final long[] end = params.getEnd();
        if (end != null && end.length > 0) {
            if (end.length > 1) {
                conditions.add(Filters.gte("position.end", end[0]));
                conditions.add(Filters.lte("position.end", end[1]));
            } else {
                conditions.add(Filters.lte("position.end", end[0]));
            }
        }

        if (aggregation == null) {
            aggregation = new ArrayList();
        }

        if (!conditions.isEmpty()) {
            aggregation.add(new BsonDocument("$match", Filters.and(conditions).toBsonDocument()));
        }
        
        return aggregation;
    }
}
