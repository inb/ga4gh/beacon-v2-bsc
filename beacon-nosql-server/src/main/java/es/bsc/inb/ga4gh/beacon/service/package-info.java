/**
 * NoSQL services that implement data queries logic.
 * 
 * Each service returns beacon results of just one type (e.g. 'individual').
 */

package es.bsc.inb.ga4gh.beacon.service;
