/**
 * *****************************************************************************
 * Copyright (C) 2024 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.vrs.model.jnosql;

import es.bsc.inb.ga4gh.vrs.model.SequenceLocation;
import es.bsc.inb.ga4gh.vrs.model.jnosql.converter.CURIEValueConverter;
import jakarta.json.bind.annotation.JsonbProperty;
import jakarta.nosql.Column;
import jakarta.nosql.Convert;
import jakarta.nosql.DiscriminatorValue;
import jakarta.nosql.Entity;

/**
 * @author Dmitry Repchevsky
 */

@Entity(SequenceLocation.TYPE)
@DiscriminatorValue(SequenceLocation.TYPE)
public class SequenceLocationEntity 
        implements SequenceLocation<CURIEEntity, SequenceLocationIntervalInterface>, 
        LocationInterface {
    
    @Column("_id")
    @Convert(CURIEValueConverter.class)
    private CURIEEntity id;
    
    @Column("sequence_id")
    @Convert(CURIEValueConverter.class)
    private CURIEEntity sequence_id;
    
    @Column("interval")
    private SequenceLocationIntervalInterface interval;
    
    @Override
    @JsonbProperty("_id")
    public CURIEEntity getId() {
        return id;
    }
    
    @Override
    @JsonbProperty("_id")
    public void setId(CURIEEntity id) {
        this.id = id;
    }
    
    @Override
    public CURIEEntity getSequenceId() {
        return sequence_id;
    }
    
    @Override
    public void setSequenceId(CURIEEntity sequence_id) {
        this.sequence_id = sequence_id;
    }
    
    @Override
    public SequenceLocationIntervalInterface getInterval() {
        return interval;
    }
    
    @Override
    public void setInterval(SequenceLocationIntervalInterface interval) {
        this.interval = interval;
    }
}
